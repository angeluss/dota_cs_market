<?php
// Yii::$app->params
return [
    'adminEmail' => 'admin@example.com',
    'langs' => [
        'ru', 'ua', 'en',
    ],
    'deflang' => 'ru',
    'lang' => 'ru'
];
