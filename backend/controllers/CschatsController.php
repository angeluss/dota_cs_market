<?php

namespace backend\controllers;

use common\models\Dcchats;
use Yii;
use common\models\Cschats;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CschatsController implements the CRUD actions for Cschats model.
 */
class CschatsController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex(){
        $chat = Cschats::find()->all();

        $banned = false;
        $request = Yii::$app->request->get();
        if(isset($request['status'])) {
            if(isset($request['message'])) {
                $user = Dcchats::find()->where(['id' => $request['message']])->one();
                if(isset($request['status'])) {
                    if($request['status']==Dcchats::STATUS_NORM) {
                        $user->status = Dcchats::STATUS_NORM;
                        $user->save();
                    } elseif($request['status']==2){
                        $user->delete();
                    }
                }
            }
            $banned = true;
            $chat = Cschats::find()->where(['status'=>Dcchats::STATUS_REPORT])->all();
        }

        return $this->render('index', [
            'chat' => $chat,
            'banned' => $banned
        ]);
    }

}
