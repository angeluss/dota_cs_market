<?php

namespace backend\controllers;

use common\models\Comments;
use common\models\NewsLang;
use common\models\RubricsNews;
use common\models\Tags;
use common\models\TagsNews;
use Yii;
use common\models\News;
use yii\data\ActiveDataProvider;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\UploadForm;
use yii\web\UploadedFile;

/**
 * NewsController implements the CRUD actions for News model.
 */
class NewsController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all News models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => News::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single News model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $news = array();
        $newsModels = NewsLang::findAll([
            'new_id' => $id,
        ]);

        foreach($newsModels as $data){
            $news[$data->lang] = $data;
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
            'news'  => $news
        ]);
    }

    /**
     * Creates a new News model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if(isset($_FILES['UploadForm'])){
            $postUpload = $_FILES['UploadForm'];
        }
        $tags = \common\models\Tags::getAllTags();
        $rubrics =  \common\models\Rubrics::getAllRubrics();
        $tags_post = Yii::$app->request->post('Tags');
        $rubrics_post = Yii::$app->request->post('Rubrics');
        $arrFiles = array();
        $model = new News();
        $upload = new UploadForm();
        $news=array();
        $langs = Yii::$app->params['langs'];

        foreach($langs as $lang){
            $news[$lang] = [
                'status' => 0,
                'lang' => $lang,
                'title' => $lang . ' title',
                'content' => $lang . ' content',
            ];
        }

        $model->created_at = time();
        $model->updated_at = time();

        if (isset($postUpload['name']['imageFile'])){
            $upload->imageFile = UploadedFile::getInstance($upload, 'imageFile');
            if ($upload->upload()) {
                $model->image_path = $postUpload['name']['imageFile'];
            }
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $conts = Yii::$app->request->post('Contents');
            if(!is_null($conts)) {
                foreach ($conts as $lang => $data) {
                    $this->saveContent($lang, $data, $model->id);
                }
            }
            if ($tags_post!=='' && $tags_post!==NULL) {
                foreach ($tags_post as $t) {
                    $news_tags = new TagsNews();
                    $news_tags->tag_id = $t;
                    $news_tags->new_id = $model->id;
                    $news_tags->save();
                }
            }

            if (!empty($rubrics_post)) {
                foreach ($rubrics_post as $t) {
                    $news_rubrics = new RubricsNews();
                    $news_rubrics->rubric_id = $t;
                    $news_rubrics->new_id = $model->id;
                    $news_rubrics->save();
                }
            }

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'upload'=>$upload,
                'news'=>$news,
                'tags'=>$tags,
                'rubrics'=>$rubrics
             ]);
        }
    }

    public function saveContent($lang, $data, $id = 0){

        $content = NewsLang::findOne([
            'new_id' => $id,
            'lang' => $lang,
        ]);

        if(is_null($content)){
            $content = new NewsLang();
            $content->new_id = $id;
            $content->lang = $lang;
        }

        $content->load($data);
        $content->status = $data['status'];
        $content->title = $data['title'];
        $content->content = $data['content'];
        $content->save();
        return $content;
    }

    /**
     * Updates an existing News model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $tags = \common\models\Tags::getAllTags();
        $rubrics =  \common\models\Rubrics::getAllRubrics();
        $tags_post = Yii::$app->request->post('Tags');
        $rubrics_post = Yii::$app->request->post('Rubrics');

        if(isset($_FILES['UploadForm'])){
            $postUpload = $_FILES['UploadForm'];
        }

        $model = News::getNew($id);
        $rubrics_for_news = \common\models\Rubrics::getAllRubricsNews($model['rubrics']);
        $tags_for_news = Tags::getAllTagsNews($model['rubrics']);
        $upload = new UploadForm();
        $model->updated_at = time();
        $news = array();
        $newsModels = NewsLang::findAll([
            'new_id' => $id,
        ]);

        foreach($newsModels as $data){
            $news[$data->lang] = [
                'status'  => $data->status,
                'title'   => $data->title,
                'content' => $data->content,
            ];
        }
        $model->updated_at = time();
        
        if (isset($postUpload['name']['imageFile'])){
            $upload->imageFile = UploadedFile::getInstance($upload, 'imageFile');
            if ($upload->upload()) {
                $model->image_path = $postUpload['name']['imageFile'];

            }
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $newslang = Yii::$app->request->post('Contents');
            if(!is_null($newslang)) {
                foreach ($newslang as $lang => $data) {
                    $this->saveContent($lang, $data, $model->id);
                }
            }

            if ($tags_post !== '' && $tags_post !== NULL) {
                TagsNews::deleteAll('new_id = :new_id', [':new_id' => $id]);
                foreach ($tags_post as $t) {
                    $news_tags = new TagsNews();
                    $news_tags->tag_id = $t;
                    $news_tags->new_id = $model->id;
                    $news_tags->save();
                }
            }

            if (!empty($rubrics_post)) {
                RubricsNews::deleteAll('new_id = :new_id', [':new_id' => $id]);
                foreach ($rubrics_post as $t) {
                    $news_rubrics = new RubricsNews();
                    $news_rubrics->rubric_id = $t;
                    $news_rubrics->new_id = $model->id;
                    $news_rubrics->save();
                }
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model'             => $model,
                'upload'            => $upload,
                'news'              => $news,
                'tags'              => $tags,
                'rubrics'           => $rubrics,
                'rubrics_for_news'  => $rubrics_for_news,
                'tags_for_news'     => $tags_for_news,
            ]);
        }
    }

    /**
     * Deletes an existing News model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
       if ($this->findModel($id)->delete()) {
           NewsLang::deleteAll('new_id = :new_id', [':new_id' => $id]);
           TagsNews::deleteAll('new_id = :new_id', [':new_id' => $id]);
           RubricsNews::deleteAll('new_id = :new_id', [':new_id' => $id]);
           Comments::deleteAll('new_id = :new_id', [':new_id' => $id]);
       }

        return $this->redirect(['index']);
    }

    /**
     * Finds the News model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return News the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = News::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
