<?php

namespace backend\controllers;

use common\models\PagesLang;
use Yii;
use common\models\Pages;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PagesController implements the CRUD actions for Pages model.
 */
class PagesController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Pages models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Pages::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Pages model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $pages = array();
        $pagesLang = PagesLang::findAll([
            'page_id' => $id,
        ]);

        foreach($pagesLang as $data){
            $pages[$data->lang] = $data;
        }

        return $this->render('view', [
            'model' => $this->findModel($id),
            'pages'=>$pages
        ]);
    }

    /**
     * Creates a new Pages model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Pages();
        $pages = array();
        $langs = Yii::$app->params['langs'];
        $conts = Yii::$app->request->post('Contents');
        foreach($langs as $lang){
            $pages[$lang] = [
                'status' => 1,
                'lang' => $lang,
                'name' => 'name '. $lang,
                'content' => 'content '.$lang,
            ];
        }
        $model->created_at = time();
        $model->updated_at = time();
        $model->page_name =$conts ? $conts[Yii::$app->params['deflang']]['name'] : '';
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if(!is_null($conts)) {
                foreach ($conts as $lang => $data) {
                    $this->saveContent($lang, $data, $model->id);
                }
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'pages'=>$pages
            ]);
        }
    }

    /**
     * Updates an existing Pages model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $pages = array();
        $langs = Yii::$app->params['langs'];
        $conts = Yii::$app->request->post('Contents');
        $model = $this->findModel($id);

        $pagesLang = PagesLang::findAll([
            'page_id' => $id,
        ]);

        foreach($pagesLang as $data){
            $pages[$data->lang] = [
                'status' => $data->status,
                'name' => $data->name,
                'content' => $data->content,
            ];
        }
        $model->updated_at = time();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if(!is_null($conts)) {
                foreach ($conts as $lang => $data) {
                    $this->saveContent($lang, $data, $model->id);
                }
            }

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'pages'=>$pages
            ]);
        }
    }

    /**
     * Deletes an existing Pages model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if ($this->findModel($id)->delete()) {
            PagesLang::deleteAll('page_id = :page_id', [':page_id' => $id]);
        }

        return $this->redirect(['index']);
    }

    public function saveContent($lang, $data, $id = 0){

        $content = PagesLang::findOne([
            'page_id' => $id,
            'lang' => $lang,
        ]);

        if(is_null($content)){
            $content = new PagesLang();
            $content->page_id = $id;
            $content->lang = $lang;
        }

        $content->load($data);
        $content->status = $data['status'];
        $content->name = $data['name'];
        $content->content = $data['content'];
        $content->save();
        return $content;
    }

    /**
     * Finds the Pages model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pages the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Pages::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
