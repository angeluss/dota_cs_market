<?php

namespace backend\controllers;

use common\models\User;
use Yii;
use common\models\Proposal;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProposalController implements the CRUD actions for Proposal model.
 */
class ProposalController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Proposal models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Proposal::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Proposal model.
     * @param integer $id
     * @return mixed
     */

    /**
     * Deletes an existing Proposal model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->status = Proposal::STATUS_CANCEL;
        if($model->save()){
            \Yii::$app->session->setFlash('success', 'Заявка помечена как отклоненная');
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Proposal model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Proposal the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Proposal::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionAccept($id){
        $model = $this->findModel($id);
        if($model->status === Proposal::STATUS_CANCEL){
            \Yii::$app->session->setFlash('success', 'Заявка уже отклонена. Принять невозможно');
        } else {
            $model->status = Proposal::STATUS_ACCEPT;
            $user = User::findOne($model->user_id);
            $user->balance -= $model->amount;
            if($model->save() && $user->save()){
                \Yii::$app->session->setFlash('success', 'Заявка помечена как принятая');
            }
        }
        return $this->redirect(['index']);
    }
}
