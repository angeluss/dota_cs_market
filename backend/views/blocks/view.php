<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Blocks */

$this->title = '№ '.$model->id;
$this->params['breadcrumbs'][] = ['label' => 'Blocks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->block_uniq;
?>
<div class="blocks-view">

    <h1><?= Html::encode('Block ' . $model->block_uniq) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <div class="nav-bars">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <?php foreach ( $blocks as $lang => $data) : ?>
                <li role="presentation" class="<?php echo isset($b) ? '' : 'active'; $b = true; ?>">
                    <a href="#<?= $lang; ?>" aria-controls="<?= $lang; ?>" role="tab" data-toggle="tab"><?= \common\models\News::getLangName($lang); ?></a>
                </li>
            <?php endforeach; ?>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <?php foreach ($blocks as $lang => $data) : ?>
                <div role="tabpanel" class="tab-pane <?php echo isset($a) ? '' : 'active'; $a = true; ?>" id="<?= $lang; ?>">
                    <?= DetailView::widget([
                        'model' => $data,
                        'attributes' => [

                            [
                                'label' => \Yii::t('app', 'Language'),
                                'value' => \common\models\News::getLangName($data->lang),
                            ],
                            [
                                'label' => \Yii::t('app', 'Включить перевод'),
                                'value' => $data->status == 1 ? 'Включен' : 'Отключен',
                            ],
                        ],
                    ]) ?>
                    <p class="lead">Содержимое блока</p>
                    <div class="well" style="background-color: white">
                        <?= $data->content; ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>


        <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'block_uniq',
            [
                'attribute' => 'Состояние блока на сайте',
                'value'=>$model->status == 1 ? 'Включен' : 'Отключен'
            ],
            [
                'attribute' => 'Создана',
                'value'=>date('Y-m-d H:i:s',$model->created_at)
            ],
            [
                'attribute' => 'Обновлена',
                'value'=>date('Y-m-d H:i:s',$model->updated_at)
            ],
        ],
    ]) ?>

</div>
