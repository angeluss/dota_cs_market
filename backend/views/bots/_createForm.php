<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Bots */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bots-form">

    <?php $form = ActiveForm::begin(); ?>

    <label class="control-label" for="bots-steam_id">Steam ID</label>
    <div id="steambots" class="form-group steambots">
        <input type="text" class="form-control" name="SteamBots[]" maxlength="255" required="required">
    </div>
    <a href="javascript:" id="addSteamIdInput">Добавить еще один</a>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>