<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \common\models\Bots;

/* @var $this yii\web\View */
/* @var $searchModel common\models\BotsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Steam bots';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bots-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Add Bot', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'steam_id',
            [
                'attribute' => 'status',
                'format'    =>'text',
                'content'   =>
                    function($data){
                        return Bots::getStatus($data->status);
                    },
            ],

            [
                'attribute' => 'created_at',
                'format'    => 'text',
                'content'   =>
                    function($data){
                        return date('Y-m-d H:i:s', $data->created_at);
                    },
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}{delete}',
            ],
        ],
    ]); ?>
</div>
