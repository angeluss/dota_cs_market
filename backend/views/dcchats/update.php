<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Dcchats */

$this->title = 'Update Dcchats: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Dcchats', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="dcchats-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
