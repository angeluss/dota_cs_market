<?php
use yii\widgets\Pjax;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Скидки на покупки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="discount-purchases-index">

    <h1><?= Html::encode($this->title) ?></h1>
<?php
    $this->registerJs(
    '
    $("#creatediscount").on("click", function() {
    $.pjax.reload({container:"#discount"});  //Reload GridView
    });'
    );
    ?>
    <div class="discount-purchases-form">


        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'amount')->textInput() ?>

        <?= $form->field($model, 'discount')->textInput() ?>
        <div class="form-group">
            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Создать'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
<!--        --><?php //Pjax::end(); ?>

    </div>
    <?php yii\widgets\Pjax::begin(['id' => 'discount']) ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'pjax'=>true,
            'export' => false,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'header'=>'Общая сумма',
                    'attribute'=>'amount',
                    'value'=>function($model) {
                            return $model->amount . ' руб.';
                        }
                ],
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'header'=>'Скидка',
                    'attribute'=>'discount',
                    'value'=>function($model) {
                            return $model->discount.' %';
                        }
                ],

                ['class' => 'yii\grid\ActionColumn',
                    'template'    => '{delete}',
                    'buttons'=>[
                    'remove' => function ($model) {
                            $urlConfig = [];
                            foreach ($model->primaryKey() as $pk) {
                                $urlConfig[$pk] = $model->$pk;
                                $urlConfig['type'] = $model->type;
                            }
                            $url = \yii\helpers\Url::toRoute(array_merge(['delete'], $urlConfig));
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                'title' => \Yii::t('yii', 'Delete'),
                                'data-confirm' => \Yii::t('yii', 'Are you sure to delete this item?'),
                                'data-method' => 'post',
                                'data-pjax' => '0',
                            ]);
                        }
                ]
                ],
            ],
        ]); ?>
    <?php Pjax::end() ?>

</div>
