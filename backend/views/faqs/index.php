<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Faqs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="faqs-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Faqs', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute'=>'question',
                'label'=>'Вопрос',
                'format'=>'text', // Возможные варианты: raw, html
                'content'=>
                    function($data){
                        $faq=\common\models\FaqContent::getQuestion($data->id);

                        return $faq->question ;
                    },
            ],
            [
                'attribute'=>'answer',
                'label'=>'Ответ',
                'format'=>'text', // Возможные варианты: raw, html
                'content'=>
                    function($data){
                        $faq=\common\models\FaqContent::getQuestion($data->id);

                        return $faq->answer ;
                    },
            ],
            [
                'attribute'=>'status',
                'label'=>'Состояние вопроса',
                'format'=>'text', // Возможные варианты: raw, html
                'content'=>
                    function($data){
                        return $data->status == 1 ? 'Включен' : 'Отключен';
                    },
            ],
            [
                'attribute'=>'created_at',
                'label'=>'Дата создания',
                'format'=>'text', // Возможные варианты: raw, html
                'content'=>
                    function($data){
                        return date('Y-m-d H:i:s', $data->created_at);
                    },
            ],
            [
                'attribute'=>'updated_at',
                'label'=>'Дата редактирования',
                'format'=>'text', // Возможные варианты: raw, html
                'content'=>
                    function($data){
                        return date('Y-m-d H:i:s', $data->updated_at);
                    },
            ],

            ['class' => 'yii\grid\ActionColumn', 'buttons'=>[
                'remove' => function ($model) {
                        $urlConfig = [];
                        foreach ($model->primaryKey() as $pk) {
                            $urlConfig[$pk] = $model->$pk;
                            $urlConfig['type'] = $model->type;
                        }
                        $url = \yii\helpers\Url::toRoute(array_merge(['delete'], $urlConfig));
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'title' => \Yii::t('yii', 'Delete'),
                            'data-confirm' => \Yii::t('yii', 'Are you sure to delete this item?'),
                            'data-method' => 'post',
                            'data-pjax' => '0',
                        ]);
                    }
            ]],
        ],
    ]); ?>

</div>
