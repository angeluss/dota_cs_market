<aside class="main-sidebar">

    <section class="sidebar">

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'Menu', 'options' => ['class' => 'header']],
                    ['label' => 'Администраторы', 'icon' => 'fa fa-user', 'url' => ['/admin'], 'visible' => Yii::$app->user->identity->role != \backend\models\Admin::ROLE_MANAGER],
                    ['label' => 'Пользователи', 'icon' => 'fa fa-user', 'url' => ['/user'], 'visible' => Yii::$app->user->identity->role != \backend\models\Admin::ROLE_MANAGER],
                    ['label' => 'Steam Боты', 'icon' => 'fa fa-steam-square', 'url' => ['/bots'], 'visible' => Yii::$app->user->identity->role != \backend\models\Admin::ROLE_MANAGER],
                    ['label' => 'Настройки', 'icon' => 'fa fa-file-code-o', 'url' => ['/settings'], 'visible' => Yii::$app->user->identity->role != \backend\models\Admin::ROLE_MANAGER],
                    ['label' => 'DB Backups', 'icon' => 'fa fa-file-code-o', 'url' => ['/backuprestore'], 'visible' => Yii::$app->user->identity->role != \backend\models\Admin::ROLE_MANAGER],
                    ['label' => 'Заявки на вывод средств', 'icon' => 'fa fa-money', 'url' => ['/proposal'], 'visible' => Yii::$app->user->identity->role != \backend\models\Admin::ROLE_MANAGER],

                    [
                        'label' => 'Торговля',
                        'icon' => 'fa fa-shopping-cart',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Сделки', 'icon' => 'fa fa-shopping-cart', 'url' => ['/trades'],],
                            ['label' => 'Промокоды', 'icon' => 'fa fa-file-code-o', 'url' => ['/promocode'],],
                            ['label' => 'Запросы на автопокупку', 'icon' => 'fa fa-file-code-o', 'url' => ['/request'],],
                        ],
                        'visible' => Yii::$app->user->identity->role != \backend\models\Admin::ROLE_MANAGER,
                    ],
                    [
                        'label' => 'Чаты',
                        'icon' => 'fa fa-share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Dota 2', 'icon' => 'fa fa-file-code-o', 'url' => ['/dcchats'],],
                            ['label' => 'CS', 'icon' => 'fa fa-cubes', 'url' => ['/cschats'],],
                        ],
                        'visible' => Yii::$app->user->identity->role != \backend\models\Admin::ROLE_MANAGER,
                    ],
                    [
                        'label' => 'CMS',
                        'icon' => 'fa fa-share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Pages', 'icon' => 'fa fa-file-code-o', 'url' => ['/pages'],],
                            ['label' => 'Blocks', 'icon' => 'fa fa-cubes', 'url' => ['/blocks'],],
                        ],
                    ],
                    [
                        'label' => 'Новости',
                        'icon' => 'fa fa-share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Рубрики', 'icon' => 'fa fa-file-code-o', 'url' => ['/rubrics'],],
                            ['label' => 'Новости', 'icon' => 'fa fa-cubes', 'url' => ['/news'],],
                            ['label' => 'Тэги', 'icon' => 'fa fa-bars', 'url' => ['/tags'],],
                        ],
                    ],
                    [
                        'label' => 'Скидки',
                        'icon' => 'fa fa-share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Скидки', 'icon' => 'fa fa-tags', 'url' => ['/discount'], 'visible' => Yii::$app->user->identity->role != \backend\models\Admin::ROLE_MANAGER],
                            ['label' => 'Комиссии', 'icon' => 'fa fa-tags', 'url' => ['/comission'], 'visible' => Yii::$app->user->identity->role != \backend\models\Admin::ROLE_MANAGER],
                        ],
                    ],
                    [
                        'label' => 'Техподдержка',
                        'icon' => 'fa fa-share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'FAQ', 'icon' => 'fa fa-tags', 'url' => ['/faqs']],
                            ['label' => 'Тикеты', 'icon' => 'fa fa-cubes', 'url' => ['/tickets'],],
                            ['label' => 'Темы', 'icon' => 'fa fa-file-code-o', 'url' => ['/themes'],],
                        ],
                    ],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                    [
                        'label' => 'Developer tools',
                        'icon' => 'fa fa-share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Gii', 'icon' => 'fa fa-file-code-o', 'url' => ['/gii'],],
                            ['label' => 'Debug', 'icon' => 'fa fa-dashboard', 'url' => ['/debug'],],
                        ],
                    ],
                ],
            ]
        ) ?>

    </section>

</aside>
