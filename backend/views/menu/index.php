<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Menu';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menu-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Menu', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute'=>'title',
                'content'=>function($data){
                    return $data->getTitle();
                }
            ],
            'title',
            [
                'attribute'=>'parent',
                'content'=>function($data){
                    return $data->getParent();
                }
            ],
            [
                'attribute'=>'status',
                'content'=>function($data){
                    return $data->status == 0 ? 'disabled' : 'enabled';
                }
            ],
            'link',
            [
                'attribute'=>'created_at',
                'label'=>'Created',
                'format'=>'datetime',
                'headerOptions' => ['width' => '200'],
            ],
            [
                'attribute'=>'updated_at',
                'label'=>'Updated',
                'format'=>'datetime',
                'headerOptions' => ['width' => '200'],
            ],

            ['class' => 'yii\grid\ActionColumn', 'buttons'=>[
        'remove' => function ($model) {
                $urlConfig = [];
                foreach ($model->primaryKey() as $pk) {
                    $urlConfig[$pk] = $model->$pk;
                    $urlConfig['type'] = $model->type;
                }
                $url = \yii\helpers\Url::toRoute(array_merge(['delete'], $urlConfig));
                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                    'title' => \Yii::t('yii', 'Delete'),
                    'data-confirm' => \Yii::t('yii', 'Are you sure to delete this item?'),
                    'data-method' => 'post',
                    'data-pjax' => '0',
                ]);
            }
    ]],
        ],
    ]); ?>

</div>
