<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\News */

$this->title =\common\models\News::getDefTitle($model->id);
$this->params['breadcrumbs'][] = ['label' => 'News', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-view">
    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <hr />
    <div class="nav-bars">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <?php foreach ( $news as $lang => $data) : ?>
                <li role="presentation" class="<?php echo isset($b) ? '' : 'active'; $b = true; ?>">
                    <a href="#<?= $lang; ?>" aria-controls="<?= $lang; ?>" role="tab" data-toggle="tab"><?= \common\models\News::getLangName($lang); ?></a>
                </li>
            <?php endforeach; ?>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <?php foreach ($news as $lang => $data) : ?>
                <div role="tabpanel" class="tab-pane <?php echo isset($a) ? '' : 'active'; $a = true; ?>" id="<?= $lang; ?>">
                    <?= DetailView::widget([
                        'model' => $data,
                        'attributes' => [
                            [
                                'label' => \Yii::t('app', 'Language'),
                                'value' => \common\models\News::getLangName($data->lang),
                            ],
                            [
                                'label' => \Yii::t('app', 'Язык на сайте'),
                                'value' => $data->status == 1 ? 'Включен' : 'Отключен',
                            ],
                        ],
                    ]) ?>
                    <p class="lead"><?= \yii::t('app', 'Заголовок новости' ) ?></p>
                    <div class="well" style="background-color: white">
                        <?= $data->title; ?>
                    </div>

                    <p class="lead"><?= yii::t('app', 'Содержимое новости' ) ?></p>
                    <div class="well" style="background-color: white">
                        <?= $data->content; ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'Состояние новости на сайте',
                    'value' => $model->status == 1 ? 'Включена' : 'Отключена'
                ],
                [
                    'attribute' => 'Создана',
                    'value'=>date('Y-m-d H:i:s', $model->created_at)
                ],
                [
                    'attribute' => 'Обновлена',
                    'value'=>date('Y-m-d H:i:s', $model->updated_at)
                ],
                'link',
                'likes',
                [
                    'label' => \Yii::t('app', 'Теги'),
                    'value' => $model->getTagsString(),
                ],
                [
                    'label' => \Yii::t('app', 'Рубрики'),
                    'value' => $model->getRubricsString(),
                ],
                [
                    'label' => \Yii::t('app', 'Главное изображение'),
                    'format' => ['image',['width'=>'200']],
                    'value' => $model->getImage('', ''),
                ],
            ],
        ]) ?>
    </div>
</div>
