<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title =  \Yii::t('app', 'Статические страницы');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pages-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Pages', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'page_name',
            [
                'attribute'=>'status',
                'label'=>'Состояние страницы',
                'format'=>'text', // Возможные варианты: raw, html
                'content'=>
                    function($data){
                        return $data->status == 1 ? 'Включена' : 'Отключена';
                    },
            ],
            'meta_title',
            'meta_desc',
            // 'alias',
            [
                'attribute'=>'created_at',
                'label'=>'Дата создания',
                'format'=>'text', // Возможные варианты: raw, html
                'content'=>
                    function($data){
                        return date('Y-m-d H:i:s', $data->created_at);
                    },
            ],
            [
                'attribute'=>'updated_at',
                'label'=>'Дата редактирования',
                'format'=>'text', // Возможные варианты: raw, html
                'content'=>
                    function($data){
                        return date('Y-m-d H:i:s', $data->updated_at);
                    },
            ],

            ['class' => 'yii\grid\ActionColumn', 'buttons'=>[
                'remove' => function ($model) {
                        $urlConfig = [];
                        foreach ($model->primaryKey() as $pk) {
                            $urlConfig[$pk] = $model->$pk;
                            $urlConfig['type'] = $model->type;
                        }
                        $url = \yii\helpers\Url::toRoute(array_merge(['delete'], $urlConfig));
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'title' => \Yii::t('yii', 'Delete'),
                            'data-confirm' => \Yii::t('yii', 'Are you sure to delete this item?'),
                            'data-method' => 'post',
                            'data-pjax' => '0',
                        ]);
                    }
            ]],
        ],
    ]); ?>

</div>
