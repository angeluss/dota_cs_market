<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PromocodeTable */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="promocode-table-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'value')->textInput() ?>
    <?= $model->isNewRecord ? $form->field($model, 'name')->textInput(['value' => \common\models\PromocodeTable::getPromoCode()]) : $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= !$model->isNewRecord ? $form->field($model, 'status')->dropDownList(\common\models\PromocodeTable::getStatus()) : ''; ?>



    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
