<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Промокоды';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="promocode-table-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать промокод', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'value',
            'name',
            'used_by',
            'used_at',
            [
                'attribute'=>'status',
                'label'=>'Состояние промокода',
                'format'=>'text', // Возможные варианты: raw, html
                'content'=>
                    function($data){
                        return $data->status == 1 ? 'Используется' : 'Новый';
                    },
            ],
            [
                'attribute'=>'created_at',
                'label'=>'Дата создания',
                'format'=>'text', // Возможные варианты: raw, html
                'content'=>
                    function($data){
                        return date('Y-m-d H:i:s', $data->created_at);
                    },
            ],

            ['class' => 'yii\grid\ActionColumn', 'buttons'=>[
                'remove' => function ($model) {
                        $urlConfig = [];
                        foreach ($model->primaryKey() as $pk) {
                            $urlConfig[$pk] = $model->$pk;
                            $urlConfig['type'] = $model->type;
                        }
                        $url = \yii\helpers\Url::toRoute(array_merge(['delete'], $urlConfig));
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'title' => \Yii::t('yii', 'Delete'),
                            'data-confirm' => \Yii::t('yii', 'Are you sure to delete this item?'),
                            'data-method' => 'post',
                            'data-pjax' => '0',
                        ]);
                    }
            ]],
        ],
    ]); ?>
</div>
