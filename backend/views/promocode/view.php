<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\PromocodeTable */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Promocode Tables', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="promocode-table-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'value',
            'name',
            'used_by',
            'used_at',
            [
                'attribute' => 'status',
                'value'=>$model->status == 1 ? 'Используется' : 'Новый'
            ],
            [
                'attribute' => 'status',
                'value'=>date('Y-m-d H:i:s', $model->created_at)
            ],
        ],
    ]) ?>

</div>
