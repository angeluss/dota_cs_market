<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Запросы на уведомление/автопокупку';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-request-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
//            'id_request',
            'user_id',
            [
                'attribute'=>'type',
                'label'=>'Тип уведомления',
                'format'=>'text', // Возможные варианты: raw, html
                'content'=>
                    function($data){
                        return \common\models\UserRequest::getType($data->type);
                    },
            ],
            'amount',

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{view}{delete}',
                'buttons'=>[
                'remove' => function ($model) {
                        $urlConfig = [];
                        foreach ($model->primaryKey() as $pk) {
                            $urlConfig[$pk] = $model->$pk;
                            $urlConfig['type'] = $model->type;
                        }
                        $url = \yii\helpers\Url::toRoute(array_merge(['delete'], $urlConfig));
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'title' => \Yii::t('yii', 'Delete'),
                            'data-confirm' => \Yii::t('yii', 'Are you sure to delete this item?'),
                            'data-method' => 'post',
                            'data-pjax' => '0',
                        ]);
                    }
            ]
            ],
        ],
    ]); ?>

</div>
