<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title =  \Yii::t('app', 'Рубрики');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rubrics-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Rubrics', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name_ru',
            'name_en',
            'name_uk',
            [
                'attribute'=>'status',
                'label'=>'Состояние рубрики',
                'format'=>'text', // Возможные варианты: raw, html
                'content'=>
                    function($data){
                        return $data->status == 1 ? 'Включена' : 'Отключена';
                    },
            ],

            ['class' => 'yii\grid\ActionColumn', 'buttons'=>[
                'remove' => function ($model) {
                        $urlConfig = [];
                        foreach ($model->primaryKey() as $pk) {
                            $urlConfig[$pk] = $model->$pk;
                            $urlConfig['type'] = $model->type;
                        }
                        $url = \yii\helpers\Url::toRoute(array_merge(['delete'], $urlConfig));
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'title' => \Yii::t('yii', 'Delete'),
                            'data-confirm' => \Yii::t('yii', 'Are you sure to delete this item?'),
                            'data-method' => 'post',
                            'data-pjax' => '0',
                        ]);
                    }
            ]],
        ],
    ]); ?>

</div>
