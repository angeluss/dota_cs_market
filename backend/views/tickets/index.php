<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tickets';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tickets-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute'=>'theme',
                'label'=>'Тема',
                'format'=>'text', // Возможные варианты: raw, html
                'content'=>
                    function($data){
                        return $data->getThemeName();
                    },
            ],
//            'text',
            [
                'attribute'=>'text',
                'format'=>'text', // Возможные варианты: raw, html
                'content'=>
                    function($data){
                        return \common\models\Tickets::cutText($data->text);
                    },
            ],
            [
                'attribute'=>'status',
                'label'=>'Состояние тикета',
                'format'=>'text', // Возможные варианты: raw, html
                'content'=>
                    function($data){
                        return \common\models\Tickets::getStatus($data->status);
                    },
            ],
            [
                'attribute'=>'created_at',
                'label'=>'Дата создания',
                'format'=>'text', // Возможные варианты: raw, html
                'content'=>
                    function($data){
                        return date('Y-m-d H:i:s', $data->created_at);
                    },
            ],
            [
                'attribute'=>'updated_at',
                'label'=>'Дата редактирования',
                'format'=>'text', // Возможные варианты: raw, html
                'content'=>
                    function($data){
                        return date('Y-m-d H:i:s', $data->updated_at);
                    },
            ],
            ['class' => 'yii\grid\ActionColumn',
                'template'    => '{view}',
            ],
        ],
    ]); ?>

</div>
