<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Trades';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trades-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label'=>'Seller',
                'format'=>'text', // ��������� ��������: raw, html
                'content'=>
                    function($data){
                        return $data->seller->username;
                    },
            ],
            [
                'label'=>'Buyer',
                'format'=>'text', // ��������� ��������: raw, html
                'content'=>
                    function($data){
                        return $data->buyer->username;
                    },
            ],
            'item_name',
            'amount',
            [
                'label'=>'Status',
                'format'=>'text', // ��������� ��������: raw, html
                'content'=>
                    function($data){
                        return \common\models\Trades::getStatus($data->status);
                    },
            ],
	    [
                'attribute'=>'date',
                'label'=>'Date',
                'format'=>'datetime',
                'headerOptions' => ['width' => '200'],
            ],
            ['class' => 'yii\grid\ActionColumn', 'template' => '{view}'],
        ],
    ]); ?>

</div>
