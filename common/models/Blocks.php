<?php

namespace common\models;

use Yii;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "blocks".
 *
 * @property integer $id
 * @property string $block_uniq
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class Blocks extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blocks';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['block_uniq'], 'required'],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['block_uniq'], 'string', 'max' => 255],
            [['block_uniq'], 'unique']
        ];
    }

    public function getRucontent(){
        return $this->hasOne(BlocksLang::className(), ['block_id' => 'id'])->where(['lang' => 'ru']);
    }

    public function getUacontent(){
        return $this->hasOne(BlocksLang::className(), ['block_id' => 'id'])->where(['lang' => 'ua']);
    }

    public function getEncontent(){
        return $this->hasOne(BlocksLang::className(), ['id' => 'block_id'])->where(['lang' => 'en']);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'block_uniq' => 'Уникальное имя блока',
            'status' => 'Состояние',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата Редактирования',
        ];
    }


    public static function getBlock($alias, $lang = 'ru'){
        $model = self::findOne(['block_uniq' => $alias]);

        switch($lang){
            case 'ru':
                return is_null($model) ? '' : $model->rucontent->content;
            case 'ua':
                return is_null($model) ? '' : $model->uacontent->content;
            case 'en':
                return is_null($model) ? '' : $model->encontent->content;
            default:
                return is_null($model) ? '' : $model->rucontent->content;
        }

    }
}
