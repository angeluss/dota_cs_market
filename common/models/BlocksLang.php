<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "blocks_lang".
 *
 * @property integer $id
 * @property integer $block_id
 * @property string $lang
 * @property string $content
 * @property integer $status
 */
class BlocksLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blocks_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['block_id', 'lang'], 'required'],
            [['block_id', 'status'], 'integer'],
            [['lang'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'block_id' => 'Block ID',
            'lang' => 'Lang',
            'content' => 'Content',
            'status' => 'Status',
        ];
    }
}
