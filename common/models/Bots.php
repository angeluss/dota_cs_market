<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "bots".
 *
 * @property integer $id
 * @property string $steam_id
 * @property integer $status
 * @property integer $created_at
 */
class Bots extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 0;
    const STATUS_DEACTIVATED = 1;
    const STATUS_BLOCKED = 2;

    public static function getStatus($status=false){
        $statuses = [
            self::STATUS_ACTIVE => 'active',
            self::STATUS_DEACTIVATED => 'deactivated',
            self::STATUS_BLOCKED => 'blocked',
        ];
        if($status === false){
            return $statuses;
        } else {
            return isset($statuses[$status]) ? $statuses[$status] : $status;
        }
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bots';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['steam_id', 'created_at'], 'required'],
            [['steam_id'], 'unique'],
            [['status', 'created_at'], 'integer'],
            [['steam_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'steam_id' => 'Steam ID',
            'status' => 'Status',
            'created_at' => 'Добавлено',
        ];
    }
}
