<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "commissions".
 *
 * @property integer $id
 * @property integer $amount
 * @property integer $overcharge
 */
class Commisions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'commissions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['amount', 'overcharge'], 'required'],
            [['amount', 'overcharge'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'amount' => 'Получено с продаж',
            'overcharge' => 'Комиссия при продаже',
        ];
    }
}
