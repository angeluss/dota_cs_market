<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "cschats".
 *
 * @property integer $id
 * @property string $nickname
 * @property string $message
 * @property integer $status
 * @property integer $date
 */
class Cschats extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cschats';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nickname', 'date'], 'required'],
            [['message'], 'string'],
            [['status', 'date'], 'integer'],
            [['nickname'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nickname' => 'Nickname',
            'message' => 'Message',
            'status' => 'Status',
            'date' => 'Date',
        ];
    }
}
