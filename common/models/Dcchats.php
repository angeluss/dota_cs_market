<?php

namespace common\models;

use Yii;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "dcchats".
 *
 * @property integer $id
 * @property string $nickname
 * @property string $message
 * @property integer $status
 * @property integer $date
 */
class Dcchats extends \yii\db\ActiveRecord
{
    const STATUS_NORM = 0;
    const STATUS_REPORT = 1;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dcchats';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nickname', 'date'], 'required'],
            [['message'], 'string'],
            [['status', 'date'], 'integer'],
            [['nickname'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nickname' => 'Nickname',
            'message' => 'Message',
            'status' => 'Status',
            'date' => 'Date',
        ];
    }

    public static function getUsersOnline() {
        define("MAX_IDLE_TIME", 3);
        if ( $directory_handle = opendir( session_save_path() ) ) {
            $count = 0;
            while ( false !== ( $file = readdir( $directory_handle ) ) ) {
                if($file != '.' && $file != '..') {
                    if(time()- filemtime(session_save_path() . DIRECTORY_SEPARATOR . $file) < MAX_IDLE_TIME * 60) {
                        $count++;
                    }
                }
            }
            closedir($directory_handle);
            return $count;
        } else {
            return false;
        }
    }

    public static function parseMsg($msg){
        $msg = strip_tags($msg);

        $msg = explode(' ', $msg );
        foreach ($msg as $key => $w){
            if (stripos($w, 'http') === 0 && !preg_match('/\s/',$w)) {

                $str = @file_get_contents($w);
                if(strlen($str) > 0){
                    $str = trim(preg_replace('/\s+/', ' ', $str)); // supports line breaks inside <title>
                    preg_match("/\<title\>(.*)\<\/title\>/i", $str, $title); // ignore case
                    if(!isset($title[1])){
                        var_dump($title); die;
                    }
                    $title = str_replace(' ', '_', $title[1]);
                } else {
                    $title = $w;
                }
                $msg[$key] =  "<a href='" . $w . "' target='_blank' style='font-weight:bold; text-decoration:underline;' >" . $title . "</a>";
            }
        }

        $msg = implode(' ', $msg);

        return $msg;
    }
}
