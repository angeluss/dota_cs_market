<?php

namespace common\models;

use Yii;
use common\models\MenuTitles;

/**
 * This is the model class for table "menu".
 *
 * @property integer $id
 * @property string $parent
 * @property integer $status
 * @property string $link
 * @property integer $created_at
 * @property integer $updated_at
 */
class Menu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['link', 'created_at', 'updated_at'], 'required'],
            [['parent', 'link'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent' => 'Parent',
            'status' => 'Status',
            'link' => 'Link',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function getTitle(){
        $tr = MenuTitles::findOne([
            'menu_id' => $this->id,
            'lang' => 'ru',
        ]);

        if(!is_null($tr)){
            $title = $tr->title;
        } else {
            $title = 'no_title';
        }
        return $title;
    }

    public function getParent(){
        $parent = Menu::findOne($this->parent);
        if(is_null($parent)){
            $parent = 'no parent';
        } else {
            $parent = $parent->getTitle();
        }
        return $parent;
    }
}
