<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "menu_titles".
 *
 * @property integer $id
 * @property integer $menu_id
 * @property string $lang
 * @property string $title
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class MenuTitles extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu_titles';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['menu_id', 'lang', 'created_at', 'updated_at'], 'required'],
            [['menu_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['lang', 'title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'menu_id' => 'Menu ID',
            'lang' => 'Lang',
            'title' => 'Title',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
