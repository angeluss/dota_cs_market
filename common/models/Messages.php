<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "messages".
 *
 * @property integer $id
 * @property integer $ticket_id
 * @property integer $sender
 * @property string $text
 * @property integer $date
 */
class Messages extends \yii\db\ActiveRecord
{
    const SENDER_OPERATOR = 2;
    const SENDER_USER = 1;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'messages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ticket_id', 'sender', 'date'], 'required'],
            [['ticket_id', 'sender', 'date'], 'integer'],
            [['text'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ticket_id' => 'Номер Тикета',
            'sender' => 'Отправитель',
            'text' => 'Текст',
            'date' => 'Дата создания',
        ];
    }
    static function getSender($sender) {
        if ($sender==self::SENDER_OPERATOR) {
            return 'оператора';
        } else {
            return 'пользователя';
        }
    }
}
