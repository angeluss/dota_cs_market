<?php

namespace common\models;

use Yii;
use yii\db\Expression;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $image_path
 * @property string $link
 * @property string $likes
 */
class News extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'created_at', 'updated_at', 'comments', 'likes'], 'integer'],
            [['created_at', 'updated_at'], 'required'],
            [['image_path', 'link'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'image_path' => 'Главное изображение',
            'link' => 'Link',
            'likes' => 'Likes',
        ];
    }

    public static function getLangName($lang)
    {
        switch ($lang){
            case 'en' :
                return \Yii::t('app', 'Английский');
            case 'ru' :
                return \Yii::t('app', 'Русский');
            case 'ua' :
                return \Yii::t('app', 'Украинский');
            default:
                return $lang;
        }
    }

    public function getNewsLangs()
    {
      return $news =NewsLang::findAll('new_id = :new_id', [':new_id' =>$this->id]);
    }

    public static function getDefTitle($id)
    {
        $news = NewsLang::find()->where(['new_id' => $id, 'lang' => Yii::$app->params['lang']])->one();
        return $news->title;
    }

    public static function getContent($id)
    {
        $news = NewsLang::find()->where(['new_id' => $id, 'lang' => Yii::$app->params['lang']])->one();
        return $news->content;
    }


    public static function getCommentsNew($id)
    {
        return Comments::find()->where(['new_id' => $id])->count();
    }

    public static function getCommentsList($id)
    {
        return Comments::find()->where(['new_id' => $id])->all();
    }

    public function getTags()
    {
        return $this->hasMany(Tags::className(), ['id' => 'tag_id'])
            ->viaTable('tags_news', ['new_id' => 'id']);
    }

    public function getTagsString()
    {
        $string = '';

        foreach($this->tags as $tag){
            $string .= $tag->name . ', ';
        }
        return $string;
    }

    public function getRubrics()
    {
        return $this->hasMany(Rubrics::className(), ['id' => 'rubric_id'])
            ->viaTable('rubrics_news', ['new_id' => 'id']);
    }

    public function getRubricsString(){
        $string = '';
        foreach($this->rubrics as $r){
            $string .= $r->name_ru . ', ';
        }
        return $string;
    }

    public function getImage($class = '', $style = ''){
        if ($this->image_path !== NULL) {
            return  \common\models\UploadForm::getDir(). '/' . $this->image_path;
        } else {
            return Yii::$app->params['frontUrl'] . 'images/noimg.png';
        }
    }

    public function getCommentaries()
    {
        return $this->hasMany(Comments::className(), ['new_id' => 'id']);
    }

    static function getBestWeekNews()
    {
       return self::find()
            ->where(['status' => News::STATUS_ACTIVE ])
            ->with('tags')
            ->orderBy([
                'likes' => SORT_DESC
            ])
            ->andWhere('created_at >= DATE_SUB(CURRENT_DATE, INTERVAL 7 DAY)')
            ->limit(3)
            ->all();
    }

    static function getBestNews()
    {
        return self::find()
            ->where(['status' => News::STATUS_ACTIVE])
            ->with('tags')
            ->orderBy([
                'likes' => SORT_DESC
            ])
            ->limit(3)
            ->all();
    }

    static function getTopNews()
    {
        return self::find()
            ->where(['status' => News::STATUS_ACTIVE])
            ->with('tags')
            ->orderBy([
                'created_at' => SORT_DESC
            ])
            ->limit(3)
            ->offset(1)
            ->asArray()
            ->all();
    }

    static function getGeneralNew()
    {
        return self::find()
            ->where(['status' => News::STATUS_ACTIVE])
            ->orderBy([
                'created_at' => SORT_DESC
            ])
            ->one();
    }

    static function getBestComments()
    {
        return self::find()
            ->where(['status' => News::STATUS_ACTIVE])
            ->with('tags')
            ->orderBy([
                'comments' => SORT_DESC
            ])
            ->limit(3)
            ->all();
    }

    static function getWeekComments()
    {
        return self::find()
            ->where(['status' => News::STATUS_ACTIVE])
            ->with('tags')
            ->orderBy([
                'comments' => SORT_DESC
            ])
            ->andWhere('created_at >= DATE_SUB(CURRENT_DATE, INTERVAL 7 DAY)')
            ->limit(3)
            ->all();
    }

    static function getNew($id)
    {
        return self::find()
            ->where(['id' =>$id])
            ->with('tags')
            ->with('rubrics')
            ->with('commentaries')
            ->one();
    }

    static function getNewsTopByGet($condition)
    {
        return self::find()
            ->where(['status' => News::STATUS_ACTIVE])
            ->with('tags')
            ->with('rubrics')
            ->orderBy([
                $condition => SORT_DESC
            ])
//            ->andWhere('created_at >= DATE_SUB(CURRENT_DATE, INTERVAL 7 DAY)')
            ->limit(50)
            ->all();
    }

    static function getNews()
    {
        return self::find()
            ->where(['status' => News::STATUS_ACTIVE])
            ->with('tags')
            ->with('rubrics')
            ->orderBy([
                'created_at' => SORT_DESC
            ])
            ->offset(4)
            ->all();
    }

    static function getSimilarNews()
    {
        return self::find()
            ->where(['status' => News::STATUS_ACTIVE])
            ->limit(6)
            ->orderBy(new Expression('rand()'))
            ->all();
    }

    static function getTagsForNew($id) {
        return self::find()
            ->where(['id' => $id])
            ->with('tags')
            ->all();
    }

}
