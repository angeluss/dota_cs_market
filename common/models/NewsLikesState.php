<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "likes_state".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $new_id
 * @property integer $positive
 * @property integer $negative
 */
class NewsLikesState extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'likes_state';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['user_id', 'new_id', 'positive', 'negative'], 'required'],
            [['user_id', 'new_id', 'positive', 'negative'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'new_id' => 'New ID',
            'positive' => 'Positive',
            'negative' => 'Negative',
        ];
    }
}
