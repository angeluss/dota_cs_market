<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "pages_lang".
 *
 * @property integer $id
 * @property integer $page_id
 * @property string $lang
 * @property string $name
 * @property string $content
 * @property integer $status
 */
class PagesLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pages_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['page_id', 'lang'], 'required'],
            [['page_id', 'status'], 'integer'],
            [['lang', 'name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'page_id' => 'Page ID',
            'lang' => 'Lang',
            'name' => 'Name',
            'content' => 'Content',
            'status' => 'Status',
        ];
    }
}
