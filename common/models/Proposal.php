<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "proposal".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $amount
 * @property integer $payment
 * @property integer $status
 * @property string $account
 * @property string $date
 */
class Proposal extends \yii\db\ActiveRecord
{
    const STATUS_CANCEL = 2;
    const STATUS_ACCEPT = 1;
    const STATUS_NEW = 0;

    const YANDEX_METHOD = 0;
    const WM_METHOD = 1;
    const QIWI_METHOD = 2;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'proposal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'amount', 'payment', 'account', 'date'], 'required'],
            [['user_id', 'amount', 'payment', 'status'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'amount' => 'Сумма для вывода',
            'payment' => 'Платежная система',
            'status' => 'Статус',
        ];
    }

    static function getStatus($status) {
        switch ($status) {
            case self::STATUS_NEW:
                return "Ожидает";
                break;
            case self::STATUS_ACCEPT:
                return "Принята";
                break;
            case self::STATUS_CANCEL:
                return "Отклонена";
                break;
            default:
                return $status;
        }
    }

    static function getPayment($type) {
        switch ($type) {
            case self::YANDEX_METHOD:
                return "Yandex";
                break;
            case self::WM_METHOD:
                return "WebMoney";
                break;
            case self::QIWI_METHOD:
                return "Qiwi";
                break;
            default:
                return $type;
        }
    }

    public function getUser(){
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

}
