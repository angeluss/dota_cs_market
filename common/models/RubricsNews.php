<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "rubrics_news".
 *
 * @property integer $id
 * @property integer $new_id
 * @property integer $rubric_id
 */
class RubricsNews extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rubrics_news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['new_id', 'rubric_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'new_id' => 'New ID',
            'rubric_id' => 'Rubric ID',
        ];
    }


}
