<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "settings".
 *
 * @property integer $id
 * @property string $meta_title
 * @property string $admin_email
 * @property string $support_email
 * @property string $skype
 * @property string $ya_acc
 * @property string $qiwi_acc
 * @property string $wm_acc
 * @property string $percent
 * @property string $vk_dota
 * @property string $vk_cs
 * @property string $youtube
 * @property integer $stop_trade
 */
class Settings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'settings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['meta_title', 'admin_email', 'support_email', 'skype', 'ya_acc', 'qiwi_acc', 'wm_acc', 'percent', 'vk_dota', 'vk_cs', 'youtube'], 'string', 'max' => 255],
            [['stop_trade'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'meta_title' => 'Meta Title',
            'admin_email' => 'Admin Email',
            'support_email' => 'Support Email',
            'skype' => 'Skype',
            'ya_acc' => 'Ya Account',
            'qiwi_acc' => 'Qiwi Account',
            'wm_acc' => 'WebMoney Account',
            'percent' => 'Percent',
            'vk_dota' => 'Vk Dota',
            'vk_cs' => 'Vk CS',
            'youtube' => 'Youtube',
            'stop_trade' => 'Оcтановить торговлю',
        ];
    }
}
