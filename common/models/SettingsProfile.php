<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "profile_settings_table".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $exchange_link
 * @property integer $timer
 * @property integer $all_pages
 * @property integer $browser
 * @property string $email
 */
class SettingsProfile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'profile_settings_table';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'timer', 'all_pages', 'browser'], 'integer'],
            [['exchange_link', 'email'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'exchange_link' => 'Exchange Link',
            'timer' => 'Timer',
            'all_pages' => 'All Pages',
            'browser' => 'Browser',
            'email' => 'Email',
        ];
    }
}
