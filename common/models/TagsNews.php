<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tags_news".
 *
 * @property integer $id
 * @property integer $new_id
 * @property integer $tag_id
 */
class TagsNews extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tags_news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['new_id', 'tag_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'new_id' => 'New ID',
            'tag_id' => 'Tag ID',
        ];
    }

    public static function getAllTagsNews($id){
        $tags = self::findAll(['new_id' => $id]);
        $tagIds = array();
        foreach($tags as $r) {

            $tagIds[]=$r->tag_id;
        }
        return $tagIds;
    }
}
