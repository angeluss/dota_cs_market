<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "trades".
 *
 * @property integer $id
 * @property integer $seller_id
 * @property integer $buyer_id
 * @property string $item_name
 * @property integer $amount
 * @property integer $status
 * @property integer $date
 */
class Trades extends \yii\db\ActiveRecord
{
    const STATUS_PROGRESS = 0;
    const STATUS_WAITING = 1;
    const STATUS_COMPLETE = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'trades';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['seller_id', 'buyer_id', 'item_name', 'amount', 'date'], 'required'],
            [['seller_id', 'buyer_id', 'amount', 'status', 'date'], 'integer'],
            [['item_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'seller_id' => 'Seller',
            'buyer_id' => 'Buyer',
            'item_name' => 'Item Name',
            'amount' => 'Amount',
            'status' => 'Status',
            'date' => 'Date',
        ];
    }

    /**
     * @param $id
     * @return string
     * get instance of User by id
     */
    public function getSeller(){
        return $this->hasOne(User::className(), ['id' => 'seller_id']);
    }

    public function getBuyer(){
        return $this->hasOne(User::className(), ['id' => 'buyer_id']);
    }

    /**
     * @param $status
     * @return string
     */
    public static function getStatus($status=false){

        $statuses = [
            self::STATUS_PROGRESS => \yii::t('app', 'in progress'),
            self::STATUS_WAITING => \yii::t('app', 'waiting'),
            self::STATUS_COMPLETE => \yii::t('app', 'completed'),
        ];
        if($status !== false) {
            return isset($statuses[$status]) ? $statuses[$status] : $status;
        } else {
            return $statuses;
        }
    }
}
