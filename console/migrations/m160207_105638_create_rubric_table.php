<?php

use yii\db\Schema;
use yii\db\Migration;

class m160207_105638_create_rubric_table extends Migration
{
    public function up()
    {
        $this->createTable('rubrics', [
            'id' => Schema::TYPE_PK,
            'name_ru' => Schema::TYPE_STRING . ' NOT NULL',
            'name_en' => Schema::TYPE_STRING . ' NOT NULL',
            'name_uk' => Schema::TYPE_STRING . ' NOT NULL',
            'status' => Schema::TYPE_BOOLEAN . ' DEFAULT 0',
        ]);

    }

    public function down()
    {
        $this->dropTable('rubrics');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
