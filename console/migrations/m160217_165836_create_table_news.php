<?php

use yii\db\Schema;
use yii\db\Migration;

class m160217_165836_create_table_news extends Migration
{
    public function up()
    {
        $this->createTable('news', [
            'id' => Schema::TYPE_PK,
            'image_path'=> $this->string(),
            'link'=> $this->string(),
            'likes'=> $this->string()->defaultValue(0),
            'created_at'=> $this->integer()->notNull(),
            'updated_at'=> $this->integer()->notNull(),
            'status' => Schema::TYPE_BOOLEAN . ' DEFAULT 0',
        ]);
    }

    public function down()
    {
        $this->dropTable('news');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
