<?php

use yii\db\Schema;
use yii\db\Migration;

class m160223_194611_creat_pages_and_blocks_tables extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('pages', [
            'id' => $this->primaryKey(),
            'page_name' => $this->string(),
            'status' => Schema::TYPE_BOOLEAN . ' DEFAULT 0',
            'meta_title' => $this->string(),
            'meta_desc' => $this->string(),
            'alias' => $this->string(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createTable('pages_lang', [
            'id' => $this->primaryKey(),
            'page_id' => $this->integer()->notNull(),
            'lang' => $this->string()->notNull(),
            'name' => $this->string()->notNull()->defaultValue(''),
            'content' => $this->string()->notNull()->defaultValue(''),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
        ], $tableOptions);

        $this->createTable('blocks', [
            'id' => $this->primaryKey(),
            'block_uniq' => $this->string()->notNull()->unique(),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createTable('blocks_lang', [
            'id' => $this->primaryKey(),
            'block_id' => $this->integer()->notNull(),
            'lang' => $this->string()->notNull(),
            'content' => $this->string()->notNull()->defaultValue(''),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('pages');
        $this->dropTable('pages_lang');
        $this->dropTable('blocks');
        $this->dropTable('pages_lang');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
