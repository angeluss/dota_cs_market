<?php

use yii\db\Schema;
use yii\db\Migration;

class m160227_195115_create_discounts_entity_tables extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('discount_purchases', [
            'id' => $this->primaryKey(),
            'amount' => $this->integer()->notNull(),
            'discount' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createTable('commissions', [
            'id' => $this->primaryKey(),
            'amount' => $this->integer()->notNull(),
            'overcharge' => $this->integer()->notNull(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('discount_purchases');
        $this->dropTable('commissions');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
