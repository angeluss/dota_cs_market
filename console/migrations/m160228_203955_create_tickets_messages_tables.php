<?php

use yii\db\Schema;
use yii\db\Migration;

class m160228_203955_create_tickets_messages_tables extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('tickets', [
            'id' => $this->primaryKey(),
            'theme' => $this->string()->notNull(),
            'text' => $this->text(),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createTable('messages', [
            'id' => $this->primaryKey(),
            'ticket_id' => $this->integer()->notNull(),
            'sender' => $this->integer()->notNull(),
            'text' => $this->text(),
            'date' => $this->integer()->notNull(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('tickets');
        $this->dropTable('messages');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
