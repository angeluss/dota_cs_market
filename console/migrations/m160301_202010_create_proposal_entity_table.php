<?php

use yii\db\Schema;
use yii\db\Migration;

class m160301_202010_create_proposal_entity_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('proposal', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'amount' => $this->integer()->notNull(),
            'payment' => $this->integer()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),

        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('proposal');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
