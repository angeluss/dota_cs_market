<?php

use yii\db\Schema;
use yii\db\Migration;

class m160326_183918_add_comment_field_news_table extends Migration
{
    public function up()
    {
        $this->alterColumn('news', 'likes', Schema::TYPE_INTEGER . '  NOT NULL');
        $this->addColumn('news', 'comments', Schema::TYPE_INTEGER . '  NOT NULL' );
    }

    public function down()
    {
        $this->dropColumn('news', 'comments');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
