<?php

use yii\db\Schema;
use yii\db\Migration;

class m160402_191917_alter_content_column_totext_table extends Migration
{
    public function up()
    {
        $this->alterColumn('blocks_lang', 'content', Schema::TYPE_TEXT. '  NOT NULL');
        $this->alterColumn('pages_lang', 'content', Schema::TYPE_TEXT. '  NOT NULL');
        $this->alterColumn('pages', 'meta_desc', Schema::TYPE_TEXT. '  NOT NULL');
    }

    public function down()
    {
        echo "m160402_191917_alter_content_column_totext_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
