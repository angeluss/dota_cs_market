<?php

use yii\db\Migration;

class m160413_062110_add_column_user_id_tickets extends Migration
{
    public function up()
    {
        $this->addColumn('tickets', 'user_id', \yii\db\oci\Schema::TYPE_INTEGER . '  NOT NULL');
    }

    public function down()
    {
        $this->dropColumn('tickets', 'user_id', \yii\db\oci\Schema::TYPE_INTEGER . '  NOT NULL');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
