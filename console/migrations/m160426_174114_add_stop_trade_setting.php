<?php

use yii\db\Migration;

class m160426_174114_add_stop_trade_setting extends Migration
{
    public function up()
    {
        $this->addColumn('settings', 'stop_trade', $this->smallInteger(1) . '  NOT NULL DEFAULT 0' );
    }

    public function down()
    {
        $this->dropColumn('settings', 'stop_trade');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
