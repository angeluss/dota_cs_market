<?php

use yii\db\Migration;

class m160426_183121_add_user_discount extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'discount', $this->integer() . '  NOT NULL DEFAULT 0' );
    }

    public function down()
    {
        $this->dropColumn('user', 'discount');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
