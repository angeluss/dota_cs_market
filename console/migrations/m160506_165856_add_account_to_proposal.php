<?php

use yii\db\Migration;

class m160506_165856_add_account_to_proposal extends Migration
{
    public function up()
    {
        $this->addColumn('proposal', 'account', $this->string() . '  NOT NULL DEFAULT ""' );
    }

    public function down()
    {
        $this->dropColumn('proposal', 'account');
    }
}
