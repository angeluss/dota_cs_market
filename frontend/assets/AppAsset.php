<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/nouislider.css',
        'css/jquery.range.css',
	    'css/styles.css',
	    '//cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css',
    ];
    public $js = [
        'js/jquery-2.2.1.min.js',
        '//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js',
        'js/nouislider.js',
        'js/dev.js',
        'js/global.js',
        'js/jquery.range.js',
        '//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
