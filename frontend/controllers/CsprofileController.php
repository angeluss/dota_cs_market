<?php
namespace frontend\controllers;

use Yii;
use yii\base\ViewContextInterface;

/**
 * Site controller
 */
class CsprofileController extends ProfileController implements ViewContextInterface
{
    public function beforeAction($action) {

        $this->view->params['layout'] = 'cs';
        Yii::$app->session->set('layout', 'cs');
        return parent::beforeAction($action);
    }

    public function getViewPath()
    {
        return Yii::getAlias('@frontend/views/profile');
    }
}
