<?php

namespace frontend\controllers;

use Yii;
use yii\base\ViewContextInterface;

class CssupportController extends SupportController implements ViewContextInterface
{

    public function beforeAction($action) {
        $this->view->params['layout'] = 'cs';
        Yii::$app->session->set('layout', 'cs');
        return parent::beforeAction($action);
    }

    public function getViewPath()
    {
        return Yii::getAlias('@frontend/views/support');
    }

}
