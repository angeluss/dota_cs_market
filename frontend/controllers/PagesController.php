<?php

namespace frontend\controllers;

use common\models\Pages;
use common\models\PagesLang;
use yii\web\HttpException;

class PagesController extends \yii\web\Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionIndex()
    {

        $model = Pages::findOne(['alias' => $_GET['view']]);
        if (!is_null($model)) {
            $pageByLang = PagesLang::findOne(['page_id'=>$model->id,'lang'=>\Yii::$app->params['lang']]);
           return $this->render('index',
                [
                    'model'=>$model,
                    'content'=>$pageByLang
                ]);
        }
        else {
            throw new HttpException(404 ,'User not found');
        }
    }
    public function actionError()
    {
        $exception = Yii::$app->errorHandler->exception;
        if ($exception !== null) {
            return $this->render('error', ['exception' => $exception]);
        }
    }
    public function actionRules()
    {
        return $this->render('rules');
    }

}
