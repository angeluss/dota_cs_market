<?php
namespace frontend\controllers;

use common\models\Faqs;
use common\models\News;
use common\models\Pages;
use common\models\PagesLang;
use common\models\Themes;
use common\models\Tickets;
use common\models\User;
use Yii;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\base\InvalidParamException;
use yii\bootstrap\Html;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * Site controller
 */
class SupportController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $tickets = new Tickets();
        if ( Yii::$app->request->post()){
            $data = Yii::$app->request->post('Tickets');
            if (isset($data)) {
                $tickets->theme = $data['theme_id'];
                $tickets->text = $data['text'];
                $tickets->created_at = time();
                $tickets->theme_id = $data['theme_id'];
                $tickets->other_theme = $data['other_theme'];
                $identity = Yii::$app->getUser()->getIdentity();
                $id_user = User::getUserIdBySteam($identity->profile['name']);
                $tickets->user_id = $id_user;
                if ($tickets->save()) {
                    Yii::$app->getSession()->setFlash('success', 'Тикет успешно создан. Следите за ним в своем инвентаре');
                  return  $this->redirect('/support/index');

                } else {
                    Yii::$app->getSession()->setFlash('error', 'Ошибка. пожалуйста, попробуйте позже. Если проблема повторится, обратитесь к администратору');
                }
            }
        }

        $themesModels = Themes::find()->all();
        $themes = [];
        foreach ($themesModels as $t){
            $themes[$t->id] = $t->name;
        }
        $themes[0] =  \yii::t('app', 'Другое');


        $faqs = Faqs::find()
            ->where(['status'=>News::STATUS_ACTIVE])
            ->with('faqContent')
            ->all();
        return $this->render('index', [
            'faqs'    => $faqs,
            'tickets' => $tickets,
            'themes'  => $themes,
        ]);
    }

}
