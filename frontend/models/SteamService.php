<?php
namespace frontend\models;

class SteamService
{
/*
 * @ToDo Move variables to Db or config file
 */
    public $steamAPIId = '8F526B6DD610D0EEE55C4E3DE6846559';
    public $site = 'http://dcmarket.loc/';
    public $openid = null;

    public static $dotaSteamId = 570;
    public static $ksSteamId = 730;


// It's main content ID for inventory for Dota and CS
    public static $inventoryId = 2;


    function __construct($needLogin) {
        if($needLogin) {
            $this->openid = new LightOpenID($this->site);
        }
    }

    /**
     * return 0 if user is not logged
     *
     * @return int
     */
    public function getIdAfterLogin()
    {
        $steamId = 0;
        if ($this->openid->validate()) {
            $id = $this->openid->identity;
            // identity is something like: http://steamcommunity.com/openid/id/76561197960435530
            // we only care about the unique account ID at the end of the URL.
            $ptn = "/^http:\/\/steamcommunity\.com\/openid\/id\/(7[0-9]{15,25}+)$/";
            preg_match($ptn, $id, $matches);
            $steamId = $matches[1];
//            echo "User is logged in (steamID: $matches[1])\n";

        }
        return $steamId;
    }

    public function getUserDetails($steamId)
    {
        $json_decoded = array();
        if($steamId) {
            $url = "http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=$this->steamAPIId&steamids=$steamId";
            $json_object = file_get_contents($url);
            $json_decoded = json_decode($json_object);
        }
        return $json_decoded;
    }

    public function showForm()
    {
        if(isset($_GET['login']))
        {
            $this->openid->identity = 'http://steamcommunity.com/openid/?l=english';
            header('Location: ' . $this->openid->authUrl());
        }
        echo '<form action="?login" method="post"><input type="image" src="http://cdn.steamcommunity.com/public/images/signinthroughsteam/sits_small.png"></form>';

    }

    /**
     * @param $gameId
     * @param int $inventoryId
     *
     * $inventoryId is taken as 2 because it's default inventory if for Dota and KS:GO
     *
     * rgInventory: shows your inventory with id, classid, instanceid, amount and pos.
    rgDescriptions: shows the descriptions of the items. The desciption has an id also (its: classid + '_' + instanceId). It also has an attribute called: icon_url.
    If you paste this icon_url into
    steamcommunity-a.akamaihd.net/economy/image/ + icon_url + /330x192
     */
    public static function getGameInventory($gameId, $inventoryId = 2)
    {
        $inventoryURL = "http://steamcommunity.com/id/userName/inventory/json/$gameId/$inventoryId";
        $inventoryContentJSON = file_get_contents($inventoryURL);
        return $inventoryContentJSON;
    }

    /**
     * @param $rgInventory
     * @param $rgDescription
     *
     * Please, pay attention. $rgDescription->$key->descriptions - can have a variable numbers of values
     *
     */
    public static function getItemsInventoryDescription($rgInventory, $rgDescription)
    {
        $newDescr = [];
        foreach($rgInventory as $item){
            $key = $item->classid . '_' . $item->instanceid;
            $newDescr[$item->id] = isset($rgDescription->$key) ? $rgDescription->$key: null;
        }
        return $newDescr;
    }

    public static function getUserInventory($userSteamId, $gameId, $inventoryId)
    {
        $inventoryURL = "https://steamcommunity.com/profiles/$userSteamId/inventory/json/$gameId/$inventoryId";
        $inventoryContentJSON = file_get_contents($inventoryURL);
        return $inventoryContentJSON;
    }

}