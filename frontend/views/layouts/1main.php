<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <link href='https://fonts.googleapis.com/css?family=Play:400,700&subset=cyrillic-ext,cyrillic,latin-ext,latin' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700&subset=cyrillic-ext,cyrillic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?= Yii::getAlias('@web'); ?>/css/styles.css" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<section id="wrapper">
    <div id="main_bg"></div>
    <header id="header">
        <div class="container clearfix">
            <div id="logo">
                <a href="/">
                    <img src="/images/logo.png" alt="Dota 2" />
                </a>
            </div>

            <nav id="main_menu" class="clearfix">
                <ul>
                    <li class="active"><a href="#" title="Предложения">Предложения</a></li>
                    <li><a href="#" title="�?нвентарь">�?нвентарь</a></li>
                    <li><a href="#" title="Техподдержка">Техподдержка</a></li>
                    <li><a href="#" title="Скидки">Скидки</a></li>
                    <li><a href="#" title="Новости">Новости</a></li>
                    <li><a href="#" title="�?нформация">�?нформация</a></li>
                </ul>
            </nav>

            <div id="header_profile">
                <div class="info">
                    <h4 class="name">Steam Steamovich</h4>
                    <div class="desc">
                        <span class="cash">1 000 P</span>
                        <span class="icon cash_icon"><i class="fa fa-money"></i></span>
                        <span class="icon">
                            <i class="fa fa-bell"></i>
                            <span class="count">5</span>
                        </span>
                    </div>
                </div>

                <div class="avatar">
                    <a href="#">
                        <img src="/images/data/avatar.jpg" alt="Avatar" />
                    </a>
                </div>
            </div>
        </div>
    </header>
    <section id="main_content">
        <?= Alert::widget() ?>
        <?= $content ?>
        </section>
</div>

<footer id="footer">
    <div class="container">
        <div class="footer_lists_holder clearfix">
            <div class="footer_list_block">
                <h3>О сайте</h3>
                <ul>
                    <li><a href="#">Как работает система?</a></li>
                    <li><a href="#">Часто задаваемые вопросы</a></li>
                    <li><a href="#">Ввод/вывод средств</a></li>
                    <li><a href="#">Техподдержка</a></li>
                </ul>
            </div>
            <div class="footer_list_block">
                <h3>Магазин</h3>
                <ul>
                    <li><a href="#">Предложения</a></li>
                    <li><a href="#">�?нвентарь</a></li>
                    <li><a href="#">Скидки</a></li>
                </ul>
            </div>
            <div class="footer_list_block">
                <h3>Соцсети</h3>
                <ul>
                    <li><a href="#">
                            <i class="fa fa-vk"></i>
                            <span>Dota2</span>
                        </a></li>
                    <li><a href="#">
                            <i class="fa fa-vk"></i>
                            <span>CS:GO</span>
                        </a></li>
                    <li><a href="#">
                            <i class="fa fa-youtube-play"></i>
                            <span>YouTube</span>
                        </a></li>
                </ul>
            </div>
            <div class="payment_block clearfix">
                <h3>Мы принимаем</h3>
                <div class="systems_holder">
                    <img src="/images/data/yandex_money.jpg" alt="Yandex Money"/>
                    <img src="/images/data/visa_wallet.jpg" alt="Visa Wallet"/>
                    <img src="/images/data/web_money.jpg" alt="Web Money"/>
                </div>
            </div>
        </div>
        <div class="copyright">
            <span>&copy; 2016 Dota2</span>
        </div>
    </div>
</footer>
</section>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
