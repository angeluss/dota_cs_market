<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <link href='https://fonts.googleapis.com/css?family=Play:400,700&subset=cyrillic-ext,cyrillic,latin-ext,latin' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700&subset=cyrillic-ext,cyrillic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="css/styles.css" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'My Company',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [
        ['label' => 'Home', 'url' => ['/site/index']],
        ['label' => 'About', 'url' => ['/site/about']],
        ['label' => 'Contact', 'url' => ['/site/contact']],
    ];
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Signup', 'url' => ['/site/signup']];
        $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
    } else {
        $menuItems[] = [
            'label' => 'Logout (' . Yii::$app->user->identity->username . ')',
            'url' => ['/site/logout'],
            'linkOptions' => ['data-method' => 'post']
        ];
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer id="footer">
    <div class="container">
        <div class="footer_lists_holder clearfix">
            <div class="footer_list_block">
                <h3>О сайте</h3>
                <ul>
                    <li><a href="#">Как работает система?</a></li>
                    <li><a href="#">Часто задаваемые вопросы</a></li>
                    <li><a href="#">Ввод/вывод средств</a></li>
                    <li><a href="#">Техподдержка</a></li>
                </ul>
            </div>
            <div class="footer_list_block">
                <h3>Магазин</h3>
                <ul>
                    <li><a href="#">Предложения</a></li>
                    <li><a href="#">Инвентарь</a></li>
                    <li><a href="#">Скидки</a></li>
                </ul>
            </div>
            <div class="footer_list_block">
                <h3>Соцсети</h3>
                <ul>
                    <li><a href="#">
                            <i class="fa fa-vk"></i>
                            <span>Dota2</span>
                        </a></li>
                    <li><a href="#">
                            <i class="fa fa-vk"></i>
                            <span>CS:GO</span>
                        </a></li>
                    <li><a href="#">
                            <i class="fa fa-youtube-play"></i>
                            <span>YouTube</span>
                        </a></li>
                </ul>
            </div>
            <div class="payment_block clearfix">
                <h3>Мы принимаем</h3>
                <div class="systems_holder">
                    <img src="images/data/yandex_money.jpg" alt="Yandex Money"/>
                    <img src="images/data/visa_wallet.jpg" alt="Visa Wallet"/>
                    <img src="images/data/web_money.jpg" alt="Web Money"/>
                </div>
            </div>
        </div>
        <div class="copyright">
            <span>&copy; 2016 Dota2</span>
        </div>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
