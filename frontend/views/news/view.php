<?php $layout = isset($this->params['layout']) ? $this->params['layout'] : ''; ?>
<?php $homePath = $layout === '' ? 'site' : $layout; ?>
<?php $user = \common\models\User::getCurrentUser(); ?>

<div class="content_holder news_list_holder">
<h1><?= \common\models\News::getDefTitle($new->id)?></h1>
<div class="meta">
    <a class="count_comments" href="">
        <i class="fa fa-comment"></i>
        <span><?= $new->comments ?></span>
    </a>
    <a class="data" href="">
        <i class="fa fa-calendar"></i>
        <span><?= date('d/m/Y H:i:s', $new['created_at']); ?></span>
    </a>
    <div class="tags">
        <span><i class="fa fa-tags"></i></span>
        <ul>
            <?php foreach ($new['tags'] as $t ) : ?>
                <li><a href="<?=\yii\helpers\Url::toRoute(['/' . $layout . 'news', 'tag'=>$t['id']]); ?>"><?= $t['name']; ?></a></li>
            <?php endforeach ?>
        </ul>
    </div>
</div>
<div class="news_article">
<div class="article_content">

    <?php if(!is_null($new->image_path)) : ?>
        <p><img src="<?= '/uploads/'.$new->image_path ?>" /></p>
    <?php endif; ?>
    <p><?= \common\models\News::getContent($new->id)?></p>
</div>
<div class="action_block clearfix">
    <div class="rate_holder">
        <div class="title">
            <h5>Оцените пост</h5>
        </div>
        <div class="rate">

                            <span class="change" onclick="setLikes(<?=$new['id']?>, 'plus', <?=$new['likes']?>, 0)">
                                <i class="fa fa-plus"></i>
                            </span>
            <span class="count" id="count_for_new<?= $new['id']?>"><?= $new['likes']>=0 ? '+'.$new['likes']  : $new['likes'] ?></span>
                            <span class="change minus" onclick="setLikes(<?=$new['id']?>, 'minus', <?=$new['likes']?>, 0 )">
                                <i class="fa fa-minus"></i>
                            </span>
        </div>
    </div>
    <div class="social_block">
        <div class="title">
            <h5>Поделитесь с друзьями</h5>
        </div>
        <div class="social">
            <ul>
                <li>
                    <a class="vk" href="javascript:"></a>
                </li>
                <li>
                    <a class="fb" href="javascript:"></a>
                </li>
                <li>
                    <a class="tw" href="javascript:"></a>
                </li>
                <li>
                    <a class="google" href="javascript:"></a>
                </li>
                <li>
                    <a class="mail_ru" href="javascript:"></a>
                </li>
                <li>
                    <a class="email" href="javascript:"></a>
                </li>
                <li>
                    <a class="add" href="javascript:"></a>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="news_top_holder similar_news">
    <div class="image_news_holder clearfix">
        <?php if(!empty($similar_news)): ?>
            <?php foreach($similar_news as $sim_new):  ?>
                <a href="<?= \Yii::$app->urlManager->createUrl(['news/view', 'id' => $sim_new['id']]); ?>">
                    <div class="image_news">
                        <div class="image_block">
                            <img src="<?=  !is_null($sim_new['image_path']) ? '/uploads/'. $sim_new['image_path'] : '/uploads/default.jpg'; ?>" alt="" />
                        </div>
                        <div class="news_desc">
                            <div class="meta">
                                <a class="count_comments" href="">
                                    <i class="fa fa-comment"></i>
                                    <span><?= $sim_new['comments'] ?></span>
                                </a>
        <!--                        <a class="user" href="">-->
        <!--                            <i class="fa fa-user"></i>-->
        <!--                            <span>Korb3n</span>-->
        <!--                        </a>-->
                            </div>
                            <h3 class="news_title"><?= \common\models\News::getDefTitle($sim_new['id'])?></h3>
                        </div>
                    </div>
                </a>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
</div>

<div class="comments_holder">
    <h3>Комментарии</h3>
    <?php foreach($comments_list as $comment):
    if($comment) { ?>

        <div class="comment clearfix">
            <div class="karma">
              <span class="change" onclick="setLikes(<?= $comment->id ?>, 'plus', <?= $comment->likes ?>, true)">
                                <i class="fa fa-plus"></i>
                            </span>
            <span class="count" id="count_for_new<?= $comment->id ?>"><?= $comment->likes>=0 ? '+'. $comment->likes  :  $comment->likes ?></span>
                            <span class="change minus" onclick="setLikes(<?=$comment->id?>, 'minus', <?=$comment->likes?>,true )">
                                <i class="fa fa-minus"></i>
                            </span>
                </div>

        <div class="comment_content">
            <div class="comment_title clearfix">
                <div class="image_block">
                    <img src="/images/data/avatar.jpg" />
                </div>
                <?php
                ?>
                <h5 class="name"><?= \common\models\User::getUserName($comment->user_id) ?></h5>
                                <span class="date">
                                    <i class="fa fa-calendar"></i>
                                    <?= date('d/m/Y H:i:s', $comment->created_at); ?>
                                </span>
            </div>
            <div class="desc">
                <p><?= $comment->message ?></p>
            </div>
        </div>
       </div>
   <?php }?>
    <?php endforeach; ?>
</div>

<!--<div class="comment_form error">-->
<!--    <div class="image_block">-->
<!--        <i class="fa fa-exclamation-triangle"></i>-->
<!--    </div>-->
<!--    <div class="error_desc">-->
<!--        <h3>Пожалуйста, авторизуйтесь!</h3>-->
<!--        <p>Чтобы оставить комментарий необходимо авторизоваться на сайте Steam - это просто и абсолютно-->
<!--            безопасно</p>-->
<!--    </div>-->
<!--</div>-->
<div class="comment_form">
    <div class="title">
        <h5>Оставте свой комментарий</h5>
    </div>

   <?php
   if (!Yii::$app->user->isGuest) {
   $form = \yii\widgets\ActiveForm::begin([
    'id' => 'comment',
    'options' => ['class' => 'form-horizontal'],
    ]) ?>
    <div class="form">
        <?= $form->field($comments, 'message')->textarea(['id'=>'comment_text'])->label('Комментарий') ?>

        <div class="form-group">
            <div class="col-lg-offset-1 col-lg-11">
                <?= \yii\helpers\Html::submitButton('Добавить комментарий', ['class' => 'btn button btn-primary']) ?>
            </div>
        </div>
        </div>
    <?php \yii\widgets\ActiveForm::end();
   } else {
       $login = '/site/login?service=steam';
       if($layout == 'cs'){
           $login = '/cs/login?service=steam';
       } ?>
       <a class="steam_holder" href="<?= $login; ?>" data-eauth-service="steam">
           <div class="steam_block clearfix">
               <div class="logo">
                   <img src="/images/steam_logo.png" alt="Steam" />
               </div>
               <div class="text">
                   <p ><?= \yii::t('app', 'Авторизоваться через '); ?><strong>Steam</strong></p>
               </div>
           </div>
       </a>
 <?php  }
    ?>
</div>
</div>
</div>
<aside class="right_sidebar">
    <h2>Популярное в ленте</h2>
    <div class="list_block">
        <div class="title">
            <h3>Лучшее</h3>
                        <span class="sorter_link">
                        <a href="<?=\yii\helpers\Url::toRoute(['/news',['sort'=>'discuss-week']]); ?>" onclick="BestWeekNews()">за неделю</a>
                    </span>
                    <span class="right_sorter_link">
                        <a href="<?=\yii\helpers\Url::toRoute(['/news',['sort'=>'best-discuss50']]); ?>">Топ-50</a>
                    </span>
        </div>
        <ul class="news_list">
          <?php  $bestNews = \common\models\News::getBestNews();
            foreach($bestNews as $best) : ?>
            <li>
                <div class="karma">
                    <span><?= $best['likes']>=0 ? '+'.$best['likes']  :  $best['likes'] ?></span>
                </div>
                <div class="list_content">
                    <h5 class="list_title">
                        <a href="<?=\Yii::$app->urlManager->createUrl(['news/view', 'id' => $best['id']])?>">
                            <?=\common\models\News::getDefTitle($best['id'])?></a>
                    </h5>
                    <div class="meta">
                        <a class="count_comments" href="">
                            <i class="fa fa-comment"></i>
                            <span><?= $best['comments']?></span>
                        </a>
                        <a class="user" href="">
                            <!--                            <i class="fa fa-user"></i>-->
                            <!--                            <span>Korb3n</span>-->
                        </a>
                        <a class="data" href="">
                            <i class="fa fa-calendar"></i>
                            <span><?= date('d/m/Y H:i:s', $best['created_at']); ?></span>
                        </a>
                    </div>
                </div>
            </li>
            <?php endforeach; ?>
        </ul>
    </div>

    <div class="list_block">
        <div class="title">
            <h3>Обсуждаемое</h3>
                 <span class="sorter_link">
                        <a href="<?=\yii\helpers\Url::toRoute(['/news',['sort'=>'discuss-week']]); ?>" onclick="BestWeekNews()">за неделю</a>
                    </span>
                    <span class="right_sorter_link">
                        <a href="<?=\yii\helpers\Url::toRoute(['/news',['sort'=>'best-discuss50']]); ?>">Топ-50</a>
                    </span>
        </div>
        <ul class="news_list">
            <?php foreach($moreComments as $com) : ?>
                <li>
                    <div class="karma">
                        <span><?= $com['likes']>=0 ? '+'.$com['likes']  :  $com['likes'] ?></span>
                    </div>
                    <div class="list_content">
                        <h5 class="list_title">
                            <a href="<?=\Yii::$app->urlManager->createUrl(['news/view', 'id' => $com['id']])?>">
                                <?=\common\models\News::getDefTitle($com['id'])?></a>
                        </h5>
                        <div class="meta">
                            <a class="count_comments" href="">
                                <i class="fa fa-comment"></i>
                                <span><?= $com['comments']?></span>
                            </a>
                            <a class="user" href="">
                                <!--                            <i class="fa fa-user"></i>-->
                                <!--                            <span>Korb3n</span>-->
                            </a>
                            <a class="data" href="">
                                <i class="fa fa-calendar"></i>
                                <span><?= date('d/m/Y H:i:s', $com['created_at']); ?></span>
                            </a>
                        </div>
                    </div>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
</aside>