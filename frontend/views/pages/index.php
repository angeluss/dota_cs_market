<?php
/**
 * Created by PhpStorm.
 * User: julia
 * Date: 3/18/16
 * Time: 8:14 PM
 * @var $this yii\web\View
 */
$this->title = $model->meta_title;
?>

<h1><?= $content->name ?></h1>
<p><?= $content->content ?></p>