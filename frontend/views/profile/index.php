<?php
$layout = isset($this->params['layout']) ? $this->params['layout'] : '';
$homePath = $layout === '' ? 'site' : $layout;
$user = \common\models\User::getCurrentUser();
?>
<?php if(!Yii::$app->user->isGuest) : ?>
    <div class="main clearfix">
        <div class="content_holder account">
            <h1><?= yii::t('app', 'Личный кабинет');?></h1>
            <div class="error_block error">
                <div class="image_block">
                    <i class="fa fa-exclamation-triangle"></i>
                </div>
                <div class="error_desc">
                    <h3><?= yii::t('app', 'Остерегайтесь мошенников');?></h3>
                    <p><?= yii::t('app', 'Чтобы оставить комментарий необходимо авторизоваться на сайте Steam - это просто и абсолютно безопасно');?></p>
                </div>
            </div>
            <div class="error_block error">
                <div class="image_block">
                    <i class="fa fa-exclamation-triangle"></i>
                </div>
                <div class="error_desc">
                    <h3><?= yii::t('app', 'Гарантийный срок хранения вещей 4 часа!');?></h3>
                    <p>
                        <?= yii::t('app', 'Чтобы оставить комментарий необходимо авторизоваться на сайте Steam - это просто и абсолютно безопасно ');?>
                        <a href="<?=\yii\helpers\Url::toRoute(['/' . $layout . 'support']); ?>">
                            <?= yii::t('app', 'техподдержка');?>
                        </a>
                    </p>
                </div>
            </div>
            <div class="good_card">
                <h3><?= yii::t('app', 'Карточка товара');?></h3>
                <div class="image_block">
                    <img src="images/data/good_image.png" alt="Card of good" />
                            <span class="good_desc top_left">
                                <strong><?= yii::t('app', 'Ценовая позиция');?></strong> <?= yii::t('app', 'предмета относительно других');?>
                            </span>
                            <span class="good_desc top_right">
                                <strong><?= yii::t('app', 'Кнопка редактирования');?></strong> <?= yii::t('app', 'вашего предмета');?>
                            </span>
                            <span class="good_desc bottom_left best_price">
                                <strong><?= yii::t('app', 'Лучшая цена');?></strong> <?= yii::t('app', 'за предмет');?>
                            </span>
                            <span class="good_desc bottom_left price">
                                <strong><?= yii::t('app', 'Ваша цена');?></strong> <?= yii::t('app', 'за предмет');?>
                            </span>
                            <span class="good_desc bottom_right">
                                <strong><?= yii::t('app', 'Категория магазина');?></strong> <?= yii::t('app', 'в которой продается предмет');?>
                            </span>
                </div>
                <div class="desc">
                    <h5><?= yii::t('app', 'Шансы на продажу');?></h5>
                    <div class="items_holder">
                        <span class="high_chance">
                            <i class="fa fa-shopping-cart"></i>
                            <?= yii::t('app', 'Высокие шансы на продажу');?>
                        </span>
                        <span class="low_chance">
                            <i class="fa fa-shopping-cart"></i>
                            <?= yii::t('app', 'Средние шансы');?>
                        </span>
                        <span class="no_chance">
                            <i class="fa fa-shopping-cart"></i>
                            <?= yii::t('app', 'Нет шансов продать с данной ценой');?>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <?= $this->render('parts/right_menu', []) ?>
    </div>

    <div class="buy_block buy_history sell">
        <div class="title_block">
            <span class="buy_icon"><i class="fa fa-shopping-basket"></i></span>
            <h3><?= yii::t('app', 'Ваши предметы на продаже');?></h3>
            <div class="button_holder">
                <button id="refresh_inventory" class="button" data-url="<?= \yii\helpers\Url::toRoute('/' . $homePath . '/refreshinv'); ?>"
                    data-game="<?= $layout === '' ? \frontend\models\SteamService::$dotaSteamId : \frontend\models\SteamService::$ksSteamId; ?>">
                    <?= yii::t('app', 'Обновить инвентарь');?>
                </button>
    <!--            <button onclick="comingSoon()" class="button">--><?//= yii::t('app', 'Остановить торговлю');?><!--</button>-->
                <button id="stop_my_trades" data-url="<?= \yii\helpers\Url::toRoute('/' . $homePath . '/stopmytrades'); ?>" class="button">
                    <?= yii::t('app', 'Снять все с продажи');?>
                </button>
            </div>
        </div>
        <div class="buy_holder clearfix">
            <?php foreach($active_trades as $i): ?>
                <div class="item">
                    <a href="">
                        <div class="image_block">
                            <img src="images/data/buy_image.png"/>
                            <div class="price">
                                <span><?= $i->amount; ?> P</span>
                                <span class="icon">
                                    <img src="images/icon_dota.png" alt="dota" />
                                </span>
                            </div>
                        </div>
                        <div class="title">
                            <span><?= $i->item_name; ?></span>
                        </div>
                    </a>
                </div>
            <?php endforeach; ?>
            <?php

                $userSteamId = Yii::$app->getUser()->getIdentity()->profile['name'];
                $gameID = $layout === '' ? \frontend\models\SteamService::$dotaSteamId : \frontend\models\SteamService::$ksSteamId;
                $userInv = \frontend\models\SteamService::getUserInventory($userSteamId, $gameID, \frontend\models\SteamService::$inventoryId);
                $userInv = json_decode($userInv);
                if (isset($userInv->rgInventory) && isset($userInv->rgDescriptions)) {
                    $items = \frontend\models\SteamService::getItemsInventoryDescription($userInv->rgInventory, $userInv->rgDescriptions);
                } else {
                    $items = false;
                }

            //    http://steamcommunity-a.akamaihd.net/economy/image/ + icon_url + /330x192
            ?>
            <?php if(\common\models\Settings::find()->one()->stop_trade === 0 && $user->status == \common\models\User::STATUS_ACTIVE) :?>
                <div class="item add_good" style="cursor: pointer">
                    <span><i class="fa fa-plus-circle"></i></span>
                    <h6><?= yii::t('app', 'Продать товар');?></h6>
                </div>
            <?php endif; ?>
        </div>
    </div>

    <div class="buy_block buy_history">
        <div class="title_block">
            <span class="buy_icon"><i class="fa fa-history"></i></span>
            <h3><?= yii::t('app', 'История ваших операций на площадке');?></h3>
            <a class="show_all" href="<?= \yii\helpers\Url::toRoute('/' . $homePath . '/statistics'); ?>"><?= yii::t('app', 'Вся история');?></a>
        </div>
        <div class="buy_holder clearfix">
            <?php foreach($self_items as $i): ?>
                <div class="item">
                    <a href="">
                        <div class="image_block">
                            <img src="images/data/buy_image.png"/>
                            <div class="price">
                                <span><?= $i->amount; ?> P</span>
                                <span class="icon">
                                    <img src="images/icon_dota.png" alt="dota" />
                                </span>
                            </div>
                        </div>
                        <div class="title">
                            <span><?= $i->item_name; ?></span>
                        </div>
                    </a>
                </div>
            <?php endforeach; ?>
            <?php if(empty($self_items)): ?>
                <span><?= yii::t('app', 'Нет сделок');?></span>
            <?php endif; ?>
        </div>
    </div>

    <?= $this->render('parts/buy_history', []) ?>

<?php else: ?>
    <h1><?= yii::t('app', 'Вы не авторизованы, пожалуйста авторизуйтесь.');?></h1>
    <div id="header_profile" style="float:none">
        <a class="steam_holder" href="/site/login?service=steam" data-eauth-service="steam">
            <div class="steam_block clearfix">
                <div class="logo">
                    <img src="/images/steam_logo.png" alt="Steam" />
                </div>
                <div class="text">
                    <p ><?= \yii::t('app', 'Авторизоваться через '); ?><strong>Steam</strong></p>
                </div>
            </div>
        </a>
    </div>
<?php endif; ?>
<div class="all_items modal-window" style="display: none">
    <div>
        <a href="javascript:" class="hide_goods" > X </a>
        <div id="inventory">
            <?= $this->render('parts/_inventory', ['items' => $items]) ?>
        </div>
        <div class="modal-item-view start_trade" style="display: none">
            <a href="javascript:" class="hide_good_prop" > X </a>
            <h3 id="tarhet_good_name"></h3>
            <img id="target_good_src" src="" alt/>
            <p><?= yii::t('app', 'Выставить на продажу с ценой'); ?>: </p>
            <p>
                <input id="spinner" name="value">
                <a href="#" id="set_price_button"><span class=""><?= yii::t('app', 'Выставить'); ?></span></a>
            </p>
            <p>
                <?= yii::t('app', 'С учётом'); ?> <a href="" target="_blank"><?= yii::t('app', 'комиссии'); ?></a> <?= yii::t('app', 'вы получите'); ?>:
                <input id="price_val_feed" style="" maxlength="5" value="10"> р.
            </p>
            <p style="float:right; ">
                <a href="#" style="font-size: 18px;"><?= yii::t('app', 'Снять с продажи'); ?></a>
            </p>
        </div>

     </div>
</div>