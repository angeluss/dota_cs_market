<?php if(false === $items): ?>
    <span><?= yii::t('app', 'Ваш инвентарь скрыт настройками приватности');?></span>
<?php else: ?>
    <?php foreach ($items as $key => $item): ?>
        <div class="item good" style="cursor: pointer">
            <img src="http://steamcommunity-a.akamaihd.net/economy/image/<?= $item->icon_url; ?>/330x192" alt />
            <div class="clearfix"></div>
            <span class="item_name">
                <?= $item->name; ?>
            </span>
        </div>
    <?php endforeach; ?>
    <?php if(empty($items)): ?>
        <span><?= yii::t('app', 'Нет товаров в инвентаре');?></span>
    <?php endif; ?>
<?php endif; ?>

