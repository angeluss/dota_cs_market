<?php

use \yii\helpers\Url;

$user = \common\models\User::getCurrentUser();
$layout = isset($this->params['layout']) ? $this->params['layout'] : '';
$homePath = $layout === '' ? 'site' : $layout;
?>
<?php
$count = \common\models\Trades::find()
    ->orderBy([
        'amount' => SORT_DESC
    ])
    ->count();
$items = \common\models\Trades::find()
    ->orderBy([
        'amount' => SORT_DESC
    ])
    ->all();
?>
<div class="buy_block buy_history">
    <div class="title_block">
        <span class="buy_icon"><i class="fa fa-shopping-cart"></i></span>
        <h3><?= yii::t('app', 'История покупок на нашей площадке'); ?></h3>
        <a class="show_all" href="<?= \yii\helpers\Url::toRoute('/' . $homePath . '/statistics'); ?>">Всего <?= \common\models\Trades::find()->count()?> покупок</a>
    </div>
    <div class="buy_holder clearfix">
        <?php if($items) : ?>
            <?php foreach($items as $item): ?>
                <div class="item">
                    <a href="">
                        <div class="image_block">
                            <img src="/images/data/buy_image.png"/>
                            <div class="price">
                                <span><?=$item->amount ?> P</span>
                                    <span class="icon">
                                        <img src="/images/icon_dota.png" alt="dota" />
                                    </span>
                            </div>
                        </div>
                        <div class="title">
                            <span><?=$item->item_name ?></span>
                        </div>
                    </a>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
</div>