<?php
use \yii\helpers\Url;

$user = \common\models\User::getCurrentUser();
$layout = isset($this->params['layout']) ? $this->params['layout'] : '';
 ?>
<aside class="right_sidebar account">
    <ul class="account_list">
        <li>
            <a href="<?= Url::toRoute(['cashout', 'id'=>$user->id]); ?>">
                <i class="fa fa-money"></i>
                <span><?= yii::t('app', 'Ввод/вывод средств'); ?></span>
            </a>
        </li>
        <li>
            <a href="<?= Url::toRoute(['requests', 'id'=>$user->id]); ?>">
                <i class="fa fa-money"></i>
                <span><?= yii::t('app', 'Список запросов на вывод средств'); ?></span>
            </a>
        </li>
        <li>
            <a href="<?= Url::toRoute(['settings', 'id'=>$user->id]); ?>">
                <i class="fa fa-cog"></i>
                <span><?= yii::t('app', 'Настройки'); ?></span>
            </a>
        </li>
        <li>
            <a href="<?= Url::toRoute(['/' . $layout . 'support#faqs']); ?>">
                <i class="fa fa-question-circle"></i>
                <span><?= yii::t('app', 'Часто задаваемые вопросы'); ?></span>
            </a>
        </li>
        <li>
            <a href="<?= Url::toRoute(['support', 'id'=>$user->id]); ?>">
                <i class="fa fa-life-ring"></i>
                <span><?= yii::t('app', 'Техподдержка'); ?></span>
            </a>
        </li>
    </ul>
</aside>