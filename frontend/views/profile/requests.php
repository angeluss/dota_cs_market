<?php

use \yii\helpers\Url;

$user = \common\models\User::getCurrentUser();
$layout = isset($this->params['layout']) ? $this->params['layout'] : '';
?>
<div class="main clearfix">
    <div class="content_holder account">
        <h1><?= yii::t('app', 'Запросы на вывод средств'); ?></h1>
        <table id="proposals-table" class="display dataTables-table" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th><?= yii::t('app', 'Дата'); ?></th>
                    <th><?= yii::t('app', 'Сумма'); ?></th>
                    <th><?= yii::t('app', 'Платежная система'); ?></th>
                    <th><?= yii::t('app', 'Аккаунт'); ?></th>
                    <th><?= yii::t('app', 'Статус'); ?></th>
                </tr>
            </thead>
            <tbody>
            <?php foreach ($requests as $request) : ?>
                <tr>
                    <td><?= date('Y-m-d', $request->date); ?></td>
                    <td><?= $request->amount ?>p.</td>
                    <td><?= \common\models\Proposal::getPayment($request->payment); ?></td>
                    <td><?= $request->account; ?></td>
                    <td><?= \common\models\Proposal::getStatus($request->status); ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <?= $this->render('parts/right_menu', []) ?>
</div>

<?= $this->render('parts/buy_history', []) ?>