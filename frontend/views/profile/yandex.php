<?php
/**
 * Created by PhpStorm.
 * User: angeluss
 * Date: 07.05.2016
 * Time: 18:32
 */
$msg = yii::t('app', 'ВНИМАНИЕ! Сумма указана с учетом комиссии');
?>

<form method="get" action="<?= \yii\helpers\Url::toRoute(['successpayment']) ?>">
    <div class="form">
        <div class="input_holder">
            <label for="amount" >Сумма</label>
            <input type="text" id="amount" name="amount" value="<?= $sum; ?>" readonly="readonly" style="width: 100px" />
        </div>
        <div class="input_holder">
            <button class="button">Пополнить</button>
        </div>
    </div>
</form>