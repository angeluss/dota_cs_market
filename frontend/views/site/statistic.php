        <h1><?= \yii::t('app', 'Статистика продаж'); ?></h1>

        <div class="buy_block">
            <div class="title_block">
                <h3><?= \yii::t('app', 'Последние покупки'); ?></h3>
            </div>
            <div class="buy_holder clearfix">
                <?php
                if($last) {
                foreach($last as $item):?>
                <div class="item">
                    <a href="">
                        <div class="image_block">
                            <img src="/images/data/buy_image.png"/>
                            <div class="price">
                                <span><?=$item->amount ?> P</span>
                                    <span class="icon">
                                        <img src="/images/icon_dota.png" alt="dota" />
                                    </span>
                            </div>
                        </div>
                        <div class="title">
                            <span><?=$item->item_name ?></span>
                        </div>
                    </a>
                </div>
                <?php endforeach;
                }
                ?>
            </div>
        </div>

        <div class="buy_block four_columns">
            <div class="title_block">
                <h3><?= \yii::t('app', 'Самые дорогие за 24 часа'); ?></h3>
                <p><?= \yii::t('app', 'Если предметов несколько, то указана средняя цена.'); ?></p>
            </div>
            <div class="buy_holder clearfix">
                <?php
                if($expensive_day) {
                foreach($expensive_day as $item): ?>
                <div class="item">
                    <a href="">
                        <div class="image_block">
                            <img src="/images/data/buy_image.png"/>
                            <div class="price">
                                <span><?=$item->amount ?> P</span>
                                    <span class="icon">
                                        <img src="/images/icon_dota.png" alt="dota" />
                                    </span>
                            </div>
                        </div>
                        <div class="title">
                            <span><?=$item->item_name ?></span>
                        </div>
                    </a>
                </div>
                <?php endforeach;
                }
                ?>
            </div>
        </div>

        <div class="buy_block three_columns">
            <div class="title_block">
                <h3><?= \yii::t('app', 'Самые дорогие за все время'); ?></h3>
            </div>
            <div class="buy_holder clearfix">
                <?php
                if($expensive_all){
                foreach($expensive_all as $item): ?>
                <div class="item">
                    <a href="">
                        <div class="image_block">
                            <img src="/images/data/buy_image.png"/>
                            <div class="price">
                                <span><?=$item->amount ?> P</span>
                                    <span class="icon">
                                        <img src="/images/icon_dota.png" alt="dota" />
                                    </span>
                            </div>
                        </div>
                        <div class="title">
                            <span><?=$item->item_name ?></span>
                        </div>
                    </a>
                </div>
                <?php endforeach;
                }
                ?>
            </div>
        </div>

        <div class="buy_block">
            <div class="title_block">
                <h3><?= \yii::t('app', 'Количество покупок в час'); ?></h3>
            </div>
            <div id="curve_chart"></div>
        </div>
        <?= $this->render('/profile/parts/buy_history') ?>
<!--        <div class="buy_block buy_history">-->
<!--            <div class="title_block">-->
<!--                <span class="buy_icon"><i class="fa fa-shopping-cart"></i></span>-->
<!--                <h3>--><?//= \yii::t('app', 'История покупок на нашей площадке'); ?><!--</h3>-->
<!--                <a class="show_all" href="#">--><?//= \yii::t('app', 'Всего ') . 9096901 . \yii::t('app', ' покупок'); ?><!--</a>-->
<!--            </div>-->
<!--            <div class="buy_holder clearfix">-->
<!--                --><?php //foreach($all as $item): ?>
<!--                <div class="item">-->
<!--                    <a href="">-->
<!--                        <div class="image_block">-->
<!--                            <img src="/images/data/buy_image.png"/>-->
<!--                            <div class="price">-->
<!--                                <span>42.48 P</span>-->
<!--                                    <span class="icon">-->
<!--                                        <img src="/images/icon_dota.png" alt="dota" />-->
<!--                                    </span>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="title">-->
<!--                            <span>Treasurae Upgrade Infuser - Winter 2016</span>-->
<!--                        </div>-->
<!--                    </a>-->
<!--                </div>-->
<!--                --><?php //endforeach; ?>
<!--            </div>-->
<!--        </div>-->
