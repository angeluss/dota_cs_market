$(document).ready(function () {
    var windowHeight = $(window).height();
    var headerHeight = $('#header').outerHeight();
    var footerHeight = $('#footer').outerHeight();

    $('#main_content').css('min-height', windowHeight - headerHeight - footerHeight);


    if($('.faq_block').size() > 0) {
        var $faqList = $('.faq_list');
        var $faqQuestion = $($faqList).find('.question');
        $($faqQuestion).on('click', function () {
            var $parent = $(this).closest('.item');
            if($($parent).find('.answer').css('display') === 'block') {
                $($parent).find('.answer').fadeOut(500);
            }
            else {
                $($parent).find('.answer').fadeIn(1500);
            }
        });
    }

    /* JS FOR CHAT */
    var $chat = $('#chat');
    var minButton = $($chat).find('.minimize');
    var modalBtn = $($chat).find('.modal_btn');
    var chatContent = $($chat).find('.chat_content');

    //chatContent.animate({
    //    scrollTop: chatContent.height()
    //}, 1500);

    chatContent.animate({ scrollTop: chatContent.prop("scrollHeight")}, 0);

    $($chat).on('mouseenter', function () {
        if($(document).height() > windowHeight) {
            $('body').addClass('no_scroll').css('padding-right', getScrollbarWidth());
            if(!$(this).hasClass('modal')) {
                $(this).css('right', 20 + getScrollbarWidth());
            }
        }
    });
    $($chat).on('mouseleave', function () {
        $('body').removeClass('no_scroll').css('padding-right', 0);
        if(!$(this).hasClass('modal')) {
            $(this).css('right', 20);
        }
    });

    minButton.on('click', function () {
        if($($chat).hasClass('close')) {
            $($chat).removeClass('close');
        }
        else {
            $($chat).addClass('close');
        }
    });
    modalBtn.on('click', function () {
        if($($chat).hasClass('modal')) {
            $($chat).removeClass('modal');
        }
        else {
            $($chat).addClass('modal');
        }
    });

    setupDragChat();

});

if($('#price_slider').size() > 0) {
    var slider = document.getElementById('price_slider');

    var html5Slider = document.getElementById('price_slider');

    noUiSlider.create(html5Slider, {
        start: [ 0, 7500 ],
        connect: true,
        range: {
            'min': [ 0 ],
            'max': [10000]
        }
    });
    var inputFrom = document.getElementById('input_from');
    var inputTo = document.getElementById('input_to');

    html5Slider.noUiSlider.on('update', function( values, handle ) {

        var value = values[handle];

        if ( handle ) {
            inputTo.value = Math.floor(value);
        } else {
            inputFrom.value = Math.floor(value);
        }
    });


    inputFrom.addEventListener('change', function(){
        html5Slider.noUiSlider.set([null, this.value]);
    });
    inputTo.addEventListener('change', function(){
        html5Slider.noUiSlider.set([null, this.value]);
    });
}


function getScrollbarWidth() {
    var outer = document.createElement("div");
    outer.style.visibility = "hidden";
    outer.style.width = "100px";
    document.body.appendChild(outer);

    var widthNoScroll = outer.offsetWidth;
    // force scrollbars
    outer.style.overflow = "scroll";

    // add innerdiv
    var inner = document.createElement("div");
    inner.style.width = "100%";
    outer.appendChild(inner);

    var widthWithScroll = inner.offsetWidth;

    // remove divs
    outer.parentNode.removeChild(outer);

    return widthNoScroll - widthWithScroll;
}


/* DRAG CHAT */
function setupDragChat() {
    var movingElement;
    var $chat = $('#chat');
    var chatPositionTop = $(window).height() - $($chat).height() - parseInt($($chat).css('bottom'), 10);
    var chatPositionLeft = $(window).width() - $($chat).width() - parseInt($($chat).css('right'), 10);
    var windowHeight = $(window).height();
    var windowWidth = $(window).width();

    $(document).on('mousedown', function (e) {
        if($($chat).hasClass('modal')) {
            return;
        }
        movingElement = e.target;
        if(movingElement === $($chat).find('.chat_header')) {
            movingElement.closest('#chat').dataset.x = e.pageX;
            movingElement.closest('#chat').dataset.y = e.pageY;
        }
    });

    $(document).on('mousemove', function (e) {
        if (movingElement) {
            var deltaX = e.pageX - movingElement.closest('#chat').dataset.x;
            var deltaY = e.pageY - movingElement.closest('#chat').dataset.y;
            var posX = parseInt(movingElement.closest('#chat').style.left || chatPositionLeft, 10) + deltaX;
            var posY = parseInt(movingElement.closest('#chat').style.top || chatPositionTop, 10) + deltaY;
            var chatWidth = $($chat).width();
            var chatHeight = $($chat).height();

            if (posX < 0) {
                posX = 0;
            }
            if (posY < 0) {
                posY = 0;
            }
            if (posX > windowWidth - chatWidth) {
                posX = windowWidth - chatWidth;
            }
            if (posY > windowHeight - chatHeight) {
                posY = windowHeight - chatHeight;
            }

            movingElement.closest('#chat').style.left = posX + 'px';
            movingElement.closest('#chat').style.top = posY + 'px';
            movingElement.closest('#chat').dataset.x = e.pageX;
            movingElement.closest('#chat').dataset.y = e.pageY;

            (function ($) {
                $.fn.disableSelection = function () {
                    return this
                        .attr('unselectable', 'on')
                        .css('user-select', 'none')
                        .on('selectstart', false);
                };
            })(jQuery);
        }
    });

    $(document).on('mouseup', function () {
        if (movingElement) {
            delete movingElement.closest('#chat').dataset.x;
            delete movingElement.closest('#chat').dataset.y;
            movingElement = null;
        }
    });
}
